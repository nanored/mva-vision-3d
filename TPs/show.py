import pylab as pl
import argparse
import math

parser = argparse.ArgumentParser(description='Plot the number of detection in function of the threshold')
parser.add_argument("folder", type=str, help='folder where the 12 files needed are stored')
folder = parser.parse_args().folder

trans = ["noise", "rotation", "scale", "viewpoint", "original"]
x0, y0, x1, y1 = 1000, 1000, 0, 0

for t in trans:
	try:
		f = open(folder+t+'.txt', "r")
	except IOError:
		print("Could not open the file given in parameter")
		exit(0)
	title = f.readline()
	n = int(f.readline())
	x, y = [0], [n-i for i in range(n+1)]
	fact = 1 if t == "noise" else 5
	tx0, tx1 = 0, 0 
	ty0, ty1 = 0, 0 
	for i in range(n):
		x.append(float(f.readline()))
		if x[-1] == x[-2]: continue
		if x[-1] - x[-2] < fact*1e-3: tx0, ty1 = x[-1], n-i
		elif tx1 == 0 and x[-1] - x[-2] > fact*2e3: tx1, ty0 = x[-1], n-i
	x0, x1 = min(x0, tx0), max(x1, tx1)
	y0, y1 = min(y0, ty0), max(y1, ty1)
	pl.plot(x, y, label=title)

pl.xlim(x0-(x1-x0)*0.02, x1)
pl.ylim(y0-(y1-y0)*0.02, y1)
pl.xlabel('Threshold')
pl.ylabel('Number of detections')
pl.legend()
pl.savefig(f'{folder}detect.png')
pl.show()