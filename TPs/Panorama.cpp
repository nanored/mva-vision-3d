// Imagine++ project
// Project:  Panorama
// Author:   Pascal Monasse
// Date:     2013/10/08

#include <Imagine/Graphics.h>
#include <Imagine/Images.h>
#include <Imagine/LinAlg.h>
#include <vector>
#include <sstream>

using namespace Imagine;
using namespace std;

// Record clicks in two images, until right button click
void getClicks(Window w1, Window w2,
			   vector<IntPoint2>& pts1, vector<IntPoint2>& pts2) {
	// ------------- TODO/A completer ----------
	IntPoint2 clickPos; // Position of the click
	Window clickWin; // Window clicked
	int subWin; // Useless ...
	while(true) {
		int button = anyGetMouse(clickPos, clickWin, subWin);
		if(button == 3) {
			// Check some conditions to stop
			if(pts1.size() != pts2.size())
				cerr << "Can't compute homography because the number of points in both windows is not the same" << endl;
			else if(pts1.size() < 4)
				cerr << "Can't compute homography because at least 4 points are needed" << endl;
			// If all conditions are respected then we return
			else return;
			continue;
		}
		if(button == 2) continue;
		// Left click
		// We add the point
		vector<IntPoint2> &pts = (clickWin == w1) ? pts1 : pts2;
		pts.push_back(clickPos);
		// And we draw the point and its index
		setActiveWindow(clickWin);
		drawCircle(clickPos, 4, RED, 3, false);
		drawString(clickPos - IntPoint2(3, 5), to_string(pts.size()), RED);
	}
	// -----------------------------------------
}

// Return homography compatible with point matches
Matrix<float> getHomography(const vector<IntPoint2>& pts1,
							const vector<IntPoint2>& pts2) {
	size_t n = min(pts1.size(), pts2.size());
	if(n<4) {
		cout << "Not enough correspondences: " << n << endl;
		return Matrix<float>::Identity(3);
	}

	Matrix<double> A(2*n,8);
	Vector<double> B(2*n);
	// ------------- TODO/A completer ----------
	for(int i = 0; i < n; i++) {
        // First row
        A(2*i, 0) = pts1[i].x();
        A(2*i, 1) = pts1[i].y();
        A(2*i, 2) = 1;
        A(2*i, 3) = 0;
        A(2*i, 4) = 0;
        A(2*i, 5) = 0;
        A(2*i, 6) = - pts2[i].x() * pts1[i].x();
        A(2*i, 7) = - pts2[i].x() * pts1[i].y();
        B[2*i] = pts2[i].x();
        // Second row
        A(2*i+1, 0) = 0;
        A(2*i+1, 1) = 0;
        A(2*i+1, 2) = 0;
        A(2*i+1, 3) = pts1[i].x();
        A(2*i+1, 4) = pts1[i].y();
        A(2*i+1, 5) = 1;
        A(2*i+1, 6) = - pts2[i].y() * pts1[i].x();
        A(2*i+1, 7) = - pts2[i].y() * pts1[i].y();
        B[2*i+1] = pts2[i].y();
	}
	// -----------------------------------------

	// Solve system and get H
	B = linSolve(A, B);
	Matrix<float> H(3, 3);
	H(0,0)=B[0]; H(0,1)=B[1]; H(0,2)=B[2];
	H(1,0)=B[3]; H(1,1)=B[4]; H(1,2)=B[5];
	H(2,0)=B[6]; H(2,1)=B[7]; H(2,2)=1;

	// Sanity check
	cout << "H.x1 cross x2 = " << endl;
	for(size_t i=0; i<n; i++) {
		float v1[]={(float)pts1[i].x(), (float)pts1[i].y(), 1.0f};
		float v2[]={(float)pts2[i].x(), (float)pts2[i].y(), 1.0f};
		Vector<float> x1(v1,3);
		Vector<float> x2(v2,3);
		x1 = H*x1;
		cout << x1[1]*x2[2]-x1[2]*x2[1] << ' '
			 << x1[2]*x2[0]-x1[0]*x2[2] << ' '
			 << x1[0]*x2[1]-x1[1]*x2[0] << endl;
	}
	return H;
}

// Grow rectangle of corners (x0,y0) and (x1,y1) to include (x,y)
void growTo(float& x0, float& y0, float& x1, float& y1, float x, float y) {
	if(x<x0) x0=x;
	if(x>x1) x1=x;
	if(y<y0) y0=y;
	if(y>y1) y1=y;    
}

// Panorama construction
void panorama(const Image<Color,2>& I1, const Image<Color,2>& I2,
			  Matrix<float> H) {
	Vector<float> v(3);
	float x0=0, y0=0, x1=I2.width(), y1=I2.height();

	// The rectangle contains the top left of the image
	v[0]=0; v[1]=0; v[2]=1;
	v=H*v; v/=v[2];
	growTo(x0, y0, x1, y1, v[0], v[1]);

	// The rectangle contains the top right of the image
	v[0]=I1.width(); v[1]=0; v[2]=1;
	v=H*v; v/=v[2];
	growTo(x0, y0, x1, y1, v[0], v[1]);

	// The rectangle contains the bottom right of the image
	v[0]=I1.width(); v[1]=I1.height(); v[2]=1;
	v=H*v; v/=v[2];
	growTo(x0, y0, x1, y1, v[0], v[1]);

	// The rectangle contains the bottom left of the image
	v[0]=0; v[1]=I1.height(); v[2]=1;
	v=H*v; v/=v[2];
	growTo(x0, y0, x1, y1, v[0], v[1]);

	cout << "x0 x1 y0 y1=" << x0 << ' ' << x1 << ' ' << y0 << ' ' << y1 << endl;

	Image<Color> I(int(x1-x0), int(y1-y0));
	setActiveWindow( openWindow(I.width(), I.height()) );

	// ------------- TODO/A completer ----------
	Matrix<float> invH = inverse(H);
	Vector<float> invV(3), v2(3);
	v[2] = 1;

	for(int i = 0; i < I.width(); i++) {
		for(int j = 0; j < I.height(); j++) {

			v[0] = x0+i; v[1] = y0+j;
			I(i, j) = BLACK;

			// If it is a pixel of I2 then use the color of I2
			if(v[0] >= 0 && v[1] >= 0 && v[0] < I2.width() && v[1] < I2.height())
				I(i, j) = I2.interpolate(v[0], v[1]);

			// Otherwise we use the color of I1 at the four pixels closest to the position invV = H^{-1}.v
			invV = invH*v; invV /= invV[2];
			RGB<double> col(0, 0, 0); // Color at (i,j) obtained with interpolation
			double sum = 0; // Factor of normalization of the interpolation
			for(int k = invV[0]; k < invV[0]+1; k++) {
				for(int l = invV[1]; l < invV[1]+1; l++) {
					v2[0] = k; v2[1] = l; v2[2] = 1;
					v2 = H*v2; v2 /= v2[2]; // Position of the pixel (k,l) of I1 in I
					double dist = norm(v2-v);
					// The more v2 is far from v the less important its color is
					if(k >= 0 && l >= 0 && k < I1.width() && l < I1.height())
						col += RGB<double>(I1(k, l)) / dist;
					sum += 1.0 / dist;
				}
			}
			
			// In the overlapping area we take the average
			if(I(i, j) != BLACK && col != BLACK) I(i, j) = (col/sum + RGB<double>(I(i, j))) / 2.0;
			// Else we just use the color of I1
			else if(I(i, j) == BLACK) I(i, j) = col / sum;

		}
	}
	// -----------------------------------------
	
	// Display and save the result
	display(I,0,0);
	save(I, "result.png");
	cout << "The result image has been saved in the file result.png" << endl;
}

// Main function
int main(int argc, char* argv[]) {
	const char* s1 = argc>1? argv[1]: srcPath("image0006.jpg");
	const char* s2 = argc>2? argv[2]: srcPath("image0007.jpg");

	// Load and display images
	Image<Color> I1, I2;
	if(!load(I1, s1) || !load(I2, s2)) {
		cerr<< "Unable to load the images" << endl;
		return 1;
	}
	Window w1 = openWindow(I1.width(), I1.height(), s1);
	display(I1, 0, 0);
	Window w2 = openWindow(I2.width(), I2.height(), s2);
	setActiveWindow(w2);
	display(I2, 0, 0);

	// Get user's clicks in images
	vector<IntPoint2> pts1, pts2;
	getClicks(w1, w2, pts1, pts2);

	vector<IntPoint2>::const_iterator it;
	cout << "pts1=" << endl;
	for(const IntPoint2 &p : pts1) cout << p << endl;
	cout << "pts2="<<endl;
	for(const IntPoint2 &p : pts2) cout << p << endl;

	// Compute homography
	Matrix<float> H = getHomography(pts1, pts2);
	cout << "H=" << H/H(2,2);

	// Apply homography
	panorama(I1, I2, H);

	endGraphics();
	return 0;
}
