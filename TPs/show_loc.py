import pylab as pl
import argparse
import matplotlib as mpl

mpl.rcParams['figure.subplot.left'] = 0.05
mpl.rcParams['figure.subplot.right'] = 0.98
mpl.rcParams['figure.subplot.bottom'] = 0.085
mpl.rcParams['figure.subplot.top'] = 0.94

parser = argparse.ArgumentParser(description='Plot the number of repeated detection in function of the threshold')
parser.add_argument("folder", type=str, help='folder where the files needed are stored')
parser.add_argument("N", type=str, help='Number of points')
args = parser.parse_args()
folder = args.folder
N = args.N

trans = ["noise", "rotation", "scale", "viewpoint"]
alg = ["", "_nms", "_anms"]
algNames = ["Harris", "NMS", "ANMS"]

na, nt = len(alg), len(trans)
fig, axs = pl.subplots(1, na, figsize=(6*na, 5))
fig2, axs2 = pl.subplots(1, nt, figsize=(5*nt, 4.5))

for i in range(na):
	for t in range(nt):
		try:
			f = open(folder+trans[t]+alg[i]+'_'+N+'_loc.txt', "r")
		except IOError:
			print(f"Could not open a required file {folder+trans[t]+alg[i]+'_'+N+'_loc.txt'}")
			exit(0)
		title = f.readline()
		n = int(f.readline())
		x = [0]
		y = [i for i in range(n+1)]
		for _ in range(n):
			x.append(float(f.readline()))
		axs[i].plot(x, y, label=trans[t])
		axs2[t].plot(x, y, label=algNames[i])

for i in range(na):
	axs[i].set_title(f'{algNames[i]} ({N} points)')
	axs[i].set_xlabel('Threshold')
	axs[i].set_ylabel('Number of repeated detections with original image')
	axs[i].legend()
for t in range(nt):
	axs2[t].set_title(f'{trans[t]} ({N} points)')
	axs2[t].set_xlabel('Threshold')
	axs2[t].set_ylabel('Number of repeated detections with original image')
	axs2[t].legend()
fig.savefig(f"{folder}loc_{N}.png")
fig2.savefig(f"{folder}locb_{N}.png")
pl.show()