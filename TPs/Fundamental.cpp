// Imagine++ project
// Project:  Fundamental
// Author:   Pascal Monasse
// Date:     2013/10/08

#include "./Imagine/Features.h"
#include <Imagine/Graphics.h>
#include <Imagine/LinAlg.h>
#include <vector>
#include <random>
using namespace Imagine;
using namespace std;

static const bool REFINIG = false; // If true the refining is used
static const double BETA = 0.001f; // Probability of failure

struct Match {
	double x1, y1, x2, y2;
};

// Display SIFT points and fill vector of point correspondences
void algoSIFT(Image<Color,2> I1, Image<Color,2> I2,
			  vector<Match>& matches) {
	// Find interest points
	SIFTDetector D;
	D.setFirstOctave(-1);
	Array<SIFTDetector::Feature> feats1 = D.run(I1);
	drawFeatures(feats1, Coords<2>(0,0));
	cout << "Im1: " << feats1.size() << flush;
	Array<SIFTDetector::Feature> feats2 = D.run(I2);
	drawFeatures(feats2, Coords<2>(I1.width(),0));
	cout << " Im2: " << feats2.size() << flush;

	const double MAX_DISTANCE = 100.0*100.0;
	for(size_t i=0; i < feats1.size(); i++) {
		SIFTDetector::Feature f1=feats1[i];
		for(size_t j=0; j < feats2.size(); j++) {
			double d = squaredDist(f1.desc, feats2[j].desc);
			if(d < MAX_DISTANCE) {
				Match m;
				m.x1 = f1.pos.x();
				m.y1 = f1.pos.y();
				m.x2 = feats2[j].pos.x();
				m.y2 = feats2[j].pos.y();
				matches.push_back(m);
			}
		}
	}
}

// Return a good normalization factor for matches
double getNormalizationFactor(vector<Match>& matches) {
	// the factor of normalization minimizes
	// SUM[on m in matches] log(fact * m.x1)^2 + ... + log(fact * m.y2)^2
	double sum = 0;
	for(const Match &m : matches) {
		sum += log(m.x1 + 1);
		sum += log(m.y1 + 1);
		sum += log(m.x2 + 1);
		sum += log(m.y2 + 1);
	}
	double fact = 1.0 / exp(sum / (4 * matches.size()));
	return fact;
}

// Return the indices of correct matches
vector<int> getInliers(const vector<Match>& matches, const FMatrix<double,3,3> F, const double distMax) {
	vector<int> inliers;
	int n = matches.size();
	for(int i = 0; i < n; i++) {
		Match m = matches[i];
		FVector<double,3> x1, x2, line;
		x1[0] = m.x1;	x1[1] = m.y1;	x1[2] = 1;
		x2[0] = m.x2;	x2[1] = m.y2;	x2[2] = 1;
		line = F * x2;
		double norm = sqrt(pow(line[0], 2.0) + pow(line[1], 2.0));
		line /= norm;
		if(abs(x1 * line) < distMax) inliers.push_back(i);
	}
	return inliers;
}

// Compute the fundamental matrix using the n first matches and using the normalization matrix N
// (Without RANSAC)
FMatrix<double,3,3> tempF(const vector<Match>& matches, int n, const FMatrix<double,3,3>& N) {
		Matrix<double> A(max(n,9), 9); // The matrix A of the linear problem
		Matrix<double> U, Vt; // Matrixes used in SVD decomposition of A
		Vector<double> S; // Eigenvalues in the SVD decomposition of A
		// We fill the last row of A with zeros if n=8
		if(n == 8)
			for(int i = 0; i < 9; i++) A(8, i) = 0;

		FMatrix<double,3,3> F; // Fundamental matrix computed in the iteration
		FMatrix<double,3,3> U2, Vt2; // Matrixes used in SVD decomposition of F
		FVector<double,3> S2; // Eigenvalues in the SVD decomposition of F

		// We compute A
		for(int i = 0; i < n; i++) {
			// Don't forget the normalization factor
			double x1 = matches[i].x1 * N(0, 0);
			double y1 = matches[i].y1 * N(1, 1);
			double x2 = matches[i].x2 * N(0, 0);
			double y2 = matches[i].y2 * N(1, 1);
			A(i, 0) = x1*x2;    A(i, 1) = x1*y2;    A(i, 2) = x1;
			A(i, 3) = y1*x2;    A(i, 4) = y1*y2;    A(i, 5) = y1;
			A(i, 6) = x2;       A(i, 7) = y2;       A(i, 8) = 1;
		}

		// f is the last row of Vt in the SVD of A
		svd(A, U, S, Vt);
		Vector<double> V9 = Vt.getRow(8);
		for(int i = 0; i < 3; i++)
			for(int j = 0; j < 3; j++)
				F(i, j) = V9[3*i+j];

		// Add the constraint det F = 0
		svd(F, U2, S2, Vt2);
		S2[2] = 0;
		F = U2 * Diagonal(S2) * Vt2;
		// Again, don't forget normalization
		F = N * F * N;

		return F;
}

// RANSAC algorithm to compute F from point matches (8-point algorithm)
// Parameter matches is filtered to keep only inliers as output.
FMatrix<double,3,3> computeF(vector<Match>& matches) {
	const double distMax = 1.15f; // Pixel error for inlier/outlier discrimination
	int Niter=100000; // Adjusted dynamically
	FMatrix<double,3,3> bestF;
	vector<Match> all=matches;
	vector<int> bestInliers;

	// --------------- TODO ------------
	// n is the number of matches
	int n = matches.size();
	if(n < 8) {
		cerr << "Not enough matches to estimate the fundamental matrix F" << endl;
		return bestF;
	}

	// Factor of normalization
	double norm_fact = getNormalizationFactor(matches);
	FVector<double,3> diagN;
	diagN[0] = norm_fact; diagN[1] = norm_fact; diagN[2] = 1.0;
	FMatrix<double,3,3> N = Diagonal(diagN);

	// Some stuff for random samples
	default_random_engine dre{random_device{}()};
	uniform_int_distribution<int> unifs[8];
	for(int i = 0; i < 8; i++)
		unifs[i] = uniform_int_distribution<int>(i, n-1);

	int niter = 0; // Number of iteration realized
	while(niter < Niter) {
		// Draw 8 matches
		for(int i = 0; i < 8; i++)
			swap(matches[i], matches[unifs[i](dre)]);
		
		// Compute F for these matches
		FMatrix<double,3,3> F = tempF(matches, 8, N);

		// Compute inliers
		vector<int> inliers = getInliers(all, F, distMax);
		if(inliers.size() > bestInliers.size()) {
			bestInliers = inliers;
			bestF = F;
			double error_prob = 1 - pow(double(inliers.size()) / double(n), 8.0);
			if(error_prob <= BETA) Niter = 1;
			else if(error_prob < 1-1e-5) Niter = log(BETA) / log(error_prob);	
			cout << "=== Iteration " << niter << " ===" << endl;
			cout << "New best model with " << inliers.size() << "/" << n << " inliers" << endl;
			cout << "The new value of Niter is " << Niter << endl;
		}

		niter ++;
	}

	// Updating matches with inliers only
	matches.clear();
	for(size_t i=0; i<bestInliers.size(); i++)
		matches.push_back(all[bestInliers[i]]);
	
	// Recompute F if the REFINING variable is set to true
	if(REFINIG)
		bestF = tempF(matches, matches.size(), N);

	/************ END OF TODO ************/

	return bestF;
}

// Expects clicks in one image and show corresponding line in other image.
// Stop at right-click.
void displayEpipolar(Image<Color> I1, Image<Color> I2,
					 const FMatrix<double,3,3>& F) {
	while(true) {
		int x,y;
		int button = getMouse(x,y);
		if(button == 3) break;
		if(button == 2) continue;
		// --------------- TODO ------------
		int w = I1.width(), w2 = I2.width();
		FVector<double, 3> point, line;
		Color c(rand()%256,rand()%256,rand()%256);
		if(x < w) {
			point[0] = x;	point[1] = y;	point[2] = 1;
			line = transpose(F) * point;
			line /= -line[1];
			drawLine(w, line[2], w+w2, line[0]*w2+line[2], c, 1);
		} else {
			point[0] = x-w;	point[1] = y;	point[2] = 1;
			line = F * point;
			line /= -line[1];
			drawLine(0, line[2], w, line[0]*w+line[2], c, 1);
		}
		drawCircle(x, y, 4, c, 3);
	}
}

int main(int argc, char* argv[]) {
	const char* s1 = argc>1? argv[1]: srcPath("im1.jpg");
	const char* s2 = argc>2? argv[2]: srcPath("im2.jpg");

	// Load and display images
	Image<Color,2> I1, I2;
	if( ! load(I1, s1) ||
		! load(I2, s2) ) {
		cerr<< "Unable to load images" << endl;
		return 1;
	}
	int w = I1.width();
	openWindow(2*w, I1.height());
	display(I1,0,0);
	display(I2,w,0);

	vector<Match> matches;
	algoSIFT(I1, I2, matches);
	cout << " matches: " << matches.size() << endl;
	click();
	
	FMatrix<double,3,3> F = computeF(matches);
	cout << "F="<< endl << F;

	// Redisplay with matches
	display(I1,0,0);
	display(I2,w,0);
	for(size_t i=0; i<matches.size(); i++) {
		Color c(rand()%256,rand()%256,rand()%256);
		fillCircle(matches[i].x1+0, matches[i].y1, 2, c);
		fillCircle(matches[i].x2+w, matches[i].y2, 2, c);        
	}
	click();

	// Redisplay without SIFT points
	display(I1,0,0);
	display(I2,w,0);
	displayEpipolar(I1, I2, F);

	endGraphics();
	return 0;
}
