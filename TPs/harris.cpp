#include <Imagine/Images.h>
#include <Imagine/LinAlg.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <assert.h>

using namespace std;
using namespace Imagine;

/************************/
/****** PARAMETERS ******/
/************************/

const static double focal = 2600;

// Standart deviation of the gaussian used for the gradient
const static double sigmaD = 1.0;
const static int kD = 2; // (2*kD+1) is the size of the convolution filter
// Standart deviation of the gaussian used for the correlation
const static double sigmaI = 4.0;
const static int kI = 5;// (2*kI+1) is the size of the convolution filter

// Constant in the Harris response
const static double kappa = 0.045;

// Threshold before applying NMS and ANMS
const static double threshold = 0.005;
// Radius for NMS
const static double rNMS = 5;
// The constant c in NMS and ANMS
const static double c = 0.9;

// Number of points to plot
const static int N = 800;

// Number of points to be considered for location error
const static int NLEs[3] = {100, 300, 600};
/************************/

// Return a 1D gaussian kernel of standard deviation sigma of size 2k+1
vector<double> gaussianKernel(double sigma, int k) {
	vector<double> K;
	double sig2 = sigma * sigma;
	double sum = 0;
	for(int x = -k; x <= k; x++) {
		double v = exp(-0.5 * x*x / sig2);
		K.push_back(v);
		sum += v;
	}
	for(int i = 0; i < 2*k+1; i++)
		K[i] /= sum;
	return K;
}

// Compute the convolution of two 1D kernels K1 and K2
vector<double> convKernels(const vector<double> &K1, const vector<double> &K2) {
	int n1 = K1.size(), n2 = K2.size();
	int k1 = (n1-1) / 2, k2 = (n2-1) / 2;
	int n = abs(n1 - n2) + 1;
	int k = (n-1) / 2;
	int jmin = k1+k2-k, jmax = k1+k2+k;
	vector<double> K(n, 0);
	for(int i = 0; i < n1; i++)
		for(int j = max(0, jmin-i); j <= min(n2-1, jmax-i); j++)
			K[i+j-jmin] += K1[i] * K2[j];
	return K;
}

// Apply a filter of kernel K on the rows of an image I
Image<double> applyFilterX(const Image<double> &I, const vector<double> &K) {
	int k = (K.size()-1) / 2;
	int w = I.width()-2*k, h = I.height();
	Image<double> IK(w, h);
	#pragma omp parallel for
	for(int x = 0; x < w; x++) {
		for(int y = 0; y < h; y++) {
			IK(x, y) = 0;
			for(int i = 0; i < 2*k+1; i++)
				IK(x, y) += I(x+i, y) * K[2*k-i];
		}
	}
	return IK;
}

// Apply a filter of kernel K on the colons of an image I
Image<double> applyFilterY(const Image<double> &I, const vector<double> &K) {
	int k = (K.size()-1) / 2;
	int w = I.width(), h = I.height()-2*k;
	Image<double> IK(w, h);
	#pragma omp parallel for
	for(int x = 0; x < w; x++) {
		for(int y = 0; y < h; y++) {
			IK(x, y) = 0;
			for(int i = 0; i < 2*k+1; i++)
				IK(x, y) += I(x, y+i) * K[2*k-i];
		}
	}
	return IK;
}

// Compute the Harris responses. R is an image of size (I.w - 2*offset, I.h - 2*offset) storing the Harris
// responses for each pixel. A pixel (i, j) in R corresponds to (i+offset, j+offset) in I.
// Returns a sorted list of pair (minus the response, pixel index). Then the first element corresponds to
// the highest response. The pixel index for the pixel (i, j) is given by i + j*w.
vector<pair<double, int>> Harris(const Image<double> &I, double threshold, Image<double> &R, int &offset) {
	int w = I.width(), h = I.height();

	// Compute Image derivatives
	vector<double> deriv = {-1.0, 0, 1.0};
	vector<double> G1 = gaussianKernel(sigmaD, kD+1);
	vector<double> GD = convKernels(deriv, G1);
	int wD = w-2*kD, hD = h-2*kD;
	Image<double> Ix = applyFilterX(I, GD);
	Image<double> Iy = applyFilterY(I, GD);

	// Compute product images
	Image<double> Ixx(wD, hD), Iyy(wD, hD), Ixy(wD, hD);
	for(int i = 0; i < wD; i++)
		for(int j = 0; j < hD; j++) {
			Ixx(i, j) = Ix(i, j+kD) * Ix(i, j+kD);
			Iyy(i, j) = Iy(i+kD, j) * Iy(i+kD, j);
			Ixy(i, j) = Ix(i, j+kD) * Iy(i+kD, j);
		}
	vector<double> GI = gaussianKernel(sigmaI, kI);
	Ixx = applyFilterX(applyFilterY(Ixx, GI), GI);
	Iyy = applyFilterX(applyFilterY(Iyy, GI), GI);
	Ixy = applyFilterX(applyFilterY(Ixy, GI), GI);
	int w2 = Ixx.width(), h2 = Ixx.height();
	offset = (w - w2) / 2;

	// Corner Response
	R.setSize(w2, h2);
	vector<pair<double, int>> responses;
	double t = 0; // Response Threshold
	// Compute responses
	for(int i = 0; i < w2; i++)
		for(int j = 0; j < h2; j++) {
			double det = Ixx(i, j) * Iyy(i, j) - Ixy(i, j) * Ixy(i, j);
			double trace = Ixx(i, j) + Iyy(i, j);
			R(i, j) = det - kappa * trace * trace;
			t = max(t, R(i, j));
		}
	t *= threshold;
	// Keep only local maximums
	for(int i = 2; i < w2; i++)
		for(int j = 2; j < h2; j++) {
				double r = R(i-1, j-1);
				if(r < t) continue;
				bool maxi = true;
				for(int k = i-2; k <= i; k++)
					for(int l = j-2; l <= j; l++)
						if(R(k, l) >= r & (k != i-1 || l != j-1)) maxi = false;
				if(maxi) responses.push_back({-r, i-1+offset + (j-1+offset)*w});
		}
	sort(responses.begin(), responses.end());
	cout << "Harris found " << responses.size() << " corner points" << endl;
	return responses;
}

// Apply the homography H to the image I and return the result. (x0, y0) are the true coordinates of the pixel (0,0)
Image<double> applyHomography(const Image<double> &I, const FMatrix<double,3,3> &H, double &x0, double &y0) {
	double w = I.width(), h = I.height();
	FMatrix<double,3,3> invH = inverse(H);

	// Compute rectangle
	x0 = 1e4, y0 = 1e4;
	double x1 = -1e4, y1 = -1e4;
	FVector<double, 3> v;
	for(double i = 0; i < 2; i++)
		for(double j = 0; j < 2; j++) {
			v[0] = i*(w-1), v[1] = j*(h-1), v[2] = 1;
			v = H*v;
			v /= v[2];
			double x = v[0], y = v[1];
			if(x<x0) x0=x;
			if(x>x1) x1=x;
			if(y<y0) y0=y;
			if(y>y1) y1=y;
		}
	int w2 = x1-x0+1, h2 = y1-y0+1;
	Image<double> I2(w2, h2);

	#pragma omp parallel for private(v)
	for(int i = 0; i < w2; i++) {
		for(int j = 0; j < h2; j++) {
			v[0] = x0+i; v[1] = y0+j, v[2] = 1;
			v = invH * v;
			v /= v[2];
			if(v[0] >= 0 && v[0] < w && v[1] >= 0 && v[1] < h)
				I2(i, j) = I.interpolate(v[0], v[1]);
			else I2(i, j) = 0;
		}
	}
	return I2;
}

// Remove the points near of the border in the Harris responses on the image transform by the homography H.
// Important because outside the image there are black pixel, then this add a gradient that doesn't exist.
// w and h are the size of the orignal image and w2 is the width of the transformed image.
void cleanResponses(vector<pair<double, int>> &points, int w2, double x0, double y0,
					int w, int h, const FMatrix<double,3,3> &H)
{
	FMatrix<double,3,3> invH = inverse(H);
	FVector<double,3> v;
	v[2] = 1;
	int n = points.size();
	int j = 0;
	for(int i = 0; i < n; i++) {
		bool keep = true;
		int pix = points[i].second;
		for(int j = 0; j < 2; j++)
			for(int k = 0; k < 2; k++) {
				v[0] = pix % w2 + x0 + (2*j-1)*(kI + kD);
				v[1] = pix / w2 + y0 + (2*k-1)*(kI + kD);
				v = invH * v;
				v /= v[2];
				if(v[0] < 0 || v[0] >= w || v[1] < 0 || v[1] >= h) {
					keep = false;
					break;
				}
			}
		if(keep) points[j++] = points[i];
	}
	points.resize(j);
}

// Return the homography corresponding to a rotation of an angle theta of the image
FMatrix<double,3,3> getRotation(double theta) {
	FMatrix<double,3,3> H;
	theta *= -1;
	H(0, 0) = cos(theta), H(0, 1) = -sin(theta), H(0, 2) = 0;
	H(1, 0) = sin(theta), H(1, 1) = cos(theta),  H(1, 2) = 0;
	H(2, 0) = 0,          H(2, 1) = 0,           H(2, 2) = 1;
	return H;
}

// Return the homography corresponding to a scale of the image
FMatrix<double,3,3> getScale(double scale) {
	FVector<double,3> d;
	d[0] = scale, d[1] =scale, d[2] = 1;
	return Diagonal(d);
}

// Return a random homography defined by a rotation of the camera
FMatrix<double,3,3> getRandomHomography(double w, double h) {
	FMatrix<double,3,3> Rx, Ry, Rz, K, H;
	Rx = getRotation(1.1 * (doubleRandom() - 0.5));
	Ry = getRotation(1.1 * (doubleRandom() - 0.5));
	Rz = getRotation(0.3 * (doubleRandom() - 0.5));
	swap(Ry(0, 1), Ry(0, 2));
	swap(Ry(1, 0), Ry(2, 0));
	swap(Ry(1, 1), Ry(2, 2));
	swap(Rx(0, 0), Rx(2, 2));
	swap(Rx(0, 1), Rx(1, 2));
	swap(Rx(1, 0), Rx(2, 1));
	K(0, 0) = focal, K(0, 1) = 0,     K(0, 2) = 0.5*w;
	K(1, 0) = 0,     K(1, 1) = focal, K(1, 2) = 0.5*h;
	K(2, 0) = 0,     K(2, 1) = 0,     K(2, 2) = 1;
	H = K * Rz * Ry * Rx * inverse(K);
	return H;
}

// Apply gaussian nois of standard deviation sigma on I
Image<double> applyNoise(const Image<double> &I, double sigma) {
	int w = I.width(), h = I.height();
	Image<double> I2(w, h);
	#pragma omp parallel for
	for(int i = 0; i < w; i++)
		for(int j = 0; j < h; j++)
			I2(i, j) = min(255.0, max(0.0, I(i, j) + sigma * gaussianRandom()));
	return I2;
}

// Convert a double image in byte image
Image<byte> convertIm(const Image<double> &I) {
	int w = I.width(), h = I.height();
	Image<byte> Ib(w, h);
	for(int i = 0; i < w; i++)
		for(int j = 0; j < h; j++)
			Ib(i, j) = I(i, j);
	return Ib;
}

// Save a double Image
bool save(const Image<double> &I, string name) {
	return save(convertIm(I), name);
}

// Apply threshold after computing responses. Usefull because we set the threshold to 0 when computing
// Harris response to plot the number of points in function of the threshold
void applyThreshold(vector<pair<double, int>> &responses) {
	double t = responses[0].first * threshold;
	int s = 1;
	while(s < int(responses.size()) && responses[s].first < t) s++;
	responses.resize(s);
	cout << "Size after applying threshold: " << s << endl;
}

// Apply NMS to the points given in first argument for an image of size w
// points in input are represented with a pair composed of the opposite of Harris response
// and an interger representing the pixel index p = x + y*w
// The function returns a subset of points
vector<pair<double, int>> NMS(const vector<pair<double, int>> &points, int w, int h,
							  const Image<double> &R, int offset)
{
	vector<pair<double, int>> res;
	int w2 = w-2*offset, h2 = h-2*offset;
	for(const auto &p : points) {
		int x = p.second % w - offset, y = p.second / w - offset;
		double r = R(x, y) / c;
		bool maxi = true;
		for(int i = max(0, x - int(rNMS)); i <= min(w2-1, x + int(rNMS)); i++) {
			int dj = sqrt(rNMS*rNMS - (i-x)*(i-x));
			for(int j = max(0, y - dj); j <= min(h2-1, y + dj); j++) {
				if(R(i, j) > r) {
					maxi = false;
					i = w2;
					break;
				}
			}
		}
		if(maxi) res.push_back(p);
	}
	return res;
}

// Apply ANMS to the points given in first argument for an image of size w
// points in input are represented with a pair composed of the opposite of Harris response
// and an interger representing the pixel index p = x + y*w
// The function returns a new sorted list of points but this times the first value of pairs
// is the opposite of the 10*radius + epsilon
vector<pair<double, int>> ANMS(const vector<pair<double, int>> &points, int w) {
	double INF = 1e9, maxHR = points[0].first;
	int n = points.size();
	vector<pair<double, int>> res;
	for(int i = 0; i < n; i++) {
		double r = INF;
		int x = points[i].second % w, y = points[i].second / w;
		for(int j = 0; j < i; j++) {
			if(points[i].first < c * points[j].first) break;
			double dx = (points[j].second % w) - x;
			double dy = (points[j].second / w) - y;
			r = min(dx*dx + dy*dy, r);
		}
		res.push_back({-10*sqrt(r) - points[i].first/maxHR, points[i].second});
	}
	sort(res.begin(), res.end());
	return res;
}

// Plot the N best points and also save image
void plotPoints(const vector<pair<double, int>> &points, int N, Image<byte> Ib, string name, double fact=1.0, int ox=0, int oy=0) {
	int w = Ib.width(), h = Ib.height();
	Image<Color> Ip(w, h);
	#pragma omp parallel for
	for(int i = 0; i < w; i++) 
		for(int j = 0; j < h; j++)
			Ip(i, j) = Color(Ib(i, j));
	for(int i = 0; i < N; i++) {
		int p = points[i].second;
		int x = p % w, y = p / w;
		drawCircle(ox + fact*x, oy + fact*y, 4, RED, 3);
		for(int a = max(0, x-3); a <= min(w-1, x+3); a++)
			for(int b = max(0, y-3); b <= min(h-1, y+3); b++)
				Ip(a, b) = Color(255, 0, 0);
	}
	save(Ip, name);
}

// Save the responses in res in a file 'name' to plot the number of detection in funcion of the threshold
bool save_responses(const vector<pair<double, int>> &res, string title) {
	int n = res.size();
	ofstream f;
	f.open(title + ".txt");
	if(!f.is_open()) return false;
	f << title << "\n" << n << "\n";
	for(int i = n-1; i >= 0; i--)
		f << - res[i].first << "\n";
	f.close();
	return true;
}

// Compute the minimal distance for the N best points of res2 multiplied by H^{-1} to the N bets points of res
bool location_error(const vector<pair<double, int>> &res, vector<pair<double, int>> &res2,
					double x0, double y0, FMatrix<double,3,3> H, int w, int w2, string title, int N)
{
	assert(N <= int(res.size()) && N <= int(res2.size()));
	
	// We sort points in function of the abcisse
	vector<pair<double, double>> points;
	for(int i = 0; i < N; i++) points.push_back({res[i].second % w, res[i].second / w});
	sort(points.begin(), points.end());
	int n = points.size();

	FVector<double,3> v;
	v[2] = 1;
	vector<double> ts;
	FMatrix<double,3,3> invH = inverse(H);
	for(int i = 0; i < N; i++) {
		v[0] = res2[i].second % w2 + x0, v[1] = res2[i].second / w2 + y0;
		v = invH * v;
		v /= v[2];
		double x = v[0], y = v[1];
		double dist = 1e8;
		// dichotomie to find the nearest point in abscisse
		int a = 0, b = n-1;
		while(a < b) {
			int c = (a + b) / 2;
			if(x < points[c].first) b = c-1;
			else a = c+1;
		}
		// Explore both sides of c
		bool stop;
		do {
			stop = true;
			if(a < n) {
				double dx = points[a].first - x;
				dx *= dx;
				if(dx < dist) {
					stop = false;
					double dy = points[a].second - y;
					dist = min(dist, dx + dy*dy);
					a ++;
				}
			}
			if(b >= 0) {
				double dx = points[b].first - x;
				dx *= dx;
				if(dx < dist) {
					stop = false;
					double dy = points[b].second - y;
					dist = min(dist, dx + dy*dy);
					b --;
				}
			}
		} while(!stop);
		ts.push_back(sqrt(dist));
	}
	sort(ts.begin(), ts.end());

	// Then save
	ofstream f;
	f.open(title + "_" + to_string(N) + "_loc.txt");
	if(!f.is_open()) return false;
	f << title << "\n" << N << "\n";
	for(double t : ts) f << t << "\n";
	f.close();
	return true;
}

// Function to compute NMS, ANMS and display results
void compute_NMS_and_display(Window W, int i, Image<byte> Ib, vector<pair<double, int>> &responses, string name,
							 vector<pair<double, int>> &res0, vector<pair<double, int>> &nms0, vector<pair<double, int>> &anms0,
							 int w, int h, const Image<double> &R, int offset, int N,
							 int w0=0, double x0=0, double y0=0, FMatrix<double,3,3> H=FMatrix<double,3,3>::Identity(),
							 double fact=1.0, int ox=0, int oy=0)
{
	setActiveWindow(W, i);
	display(Ib, ox, oy, false, fact);
	plotPoints(responses, N, Ib, name+".jpeg", fact, ox, oy);
	save_responses(responses, name);
	applyThreshold(responses);
	if(res0.empty()) res0 = responses;
	else for(int NLE : NLEs)
		location_error(res0, responses, x0, y0, H, w0, w, name, NLE);

	// best NMS responses
	setActiveWindow(W, i+5);
	display(Ib, ox, oy, false, fact);
	vector<pair<double, int>> nms = NMS(responses, w, h, R, offset);
	plotPoints(nms, N, Ib, name+"_nms.jpeg", fact, ox, oy);
	if(nms0.empty()) nms0 = nms;
	else  for(int NLE : NLEs)
		location_error(nms0, nms, x0, y0, H, w0, w, name+"_nms", NLE);

	// best ANMS responses
	setActiveWindow(W, i+10);
	display(Ib, ox, oy, false, fact);
	vector<pair<double, int>> anms = ANMS(responses, w);
	plotPoints(anms, N, Ib, name+"_anms.jpeg", fact, ox, oy);
	if(anms0.empty()) anms0 = anms;
	else  for(int NLE : NLEs)
		location_error(anms0, anms, x0, y0, H, w0, w, name+"_anms", NLE);
}

int main(int argc, char* argv[]) {
	if(argc != 2) {
		cerr << "Exactly one argument needed, corresponding to the path of the image" << endl;
		return 1;
	}
	initRandom();
	Image<byte> I0;
	if(! load(I0, argv[1])) {
		cerr << "Unable to load the image !" << endl;
		return 1;
	}
	int w = I0.width(), h = I0.height();
	Image<double> I(w, h);
	for(int i = 0; i < w; i++)
		for(int j = 0; j < h; j++)
			I(i, j) = I0(i, j);
	
	// Display
	string tabNames[5*3];
	tabNames[0] = "Original", tabNames[1] = "Rotation", tabNames[2] = "Noise", tabNames[3] = "Scale", tabNames[4] = "Viewpoint";
	for(int i = 0; i < 5; i++) {
		tabNames[i+5] = tabNames[i] + " NMS";
		tabNames[i+10] = tabNames[i] + " ANMS";
	}
	Window W = openComplexWindow(w, h, "Harris Detection", 5*3, tabNames);

	// Some variables
	Image<double> R; // Image of responses
	int offset; // Offset between original image and R
	vector<pair<double, int>> responses, responses0, nms0, anms0; // Sorted responses
	FMatrix<double,3,3> H; // Homography
	Image<double> I2; // Transformed image
	Image<byte> I2b; // Byte image to display result
	double x0, y0; // true coordinates of pixel (0, 0) in I2
	int w2, h2; // Size of I2
	double fact; // factor of display for I2
	int ox, oy; // display offset for I2

	// best Harris responses
	cout << "Original" << endl;
	responses = Harris(I, 0, R, offset);
	compute_NMS_and_display(W, 0, I0, responses, "original",
							responses0, nms0, anms0, w, h, R, offset, N);

	// best Harris responses on rotated image
	cout << "Rotation" << endl;
	double angle = 0.4;
	H = getRotation(angle);
	I2 = applyHomography(I, H, x0, y0);
	I2b = convertIm(I2);
	w2 = I2.width(), h2 = I2.height();
	fact = min(double(w) / double(w2), double(h) / double(h2));
	ox = 0.5 * (w - w2*fact), oy = 0.5 * (h - h2*fact);
	responses = Harris(I2, 0, R, offset);
	cleanResponses(responses, w2, x0, y0, w, h, H);
	compute_NMS_and_display(W, 1, I2b, responses, "rotation", responses0, nms0, anms0,
							w2, h2, R, offset, N, w, x0, y0, H, fact, ox, oy);
	
	// best Harris responses on noised image
	cout << "Noise" << endl;
	H = FMatrix<double,3,3>::Identity();
	double sigma = 60;
	I2 = applyNoise(I, sigma);
	I2b = convertIm(I2);
	responses = Harris(I2, 0, R, offset);
	compute_NMS_and_display(W, 2, I2b, responses, "noise", responses0, nms0, anms0,
							w, h, R, offset, N, w, 0, 0, H);

	// best Harris responses on scaled image
	cout << "Scale" << endl;
	double scale = 0.6;
	H = getScale(scale);
	I2 = applyHomography(I, H, x0, y0);
	I2b = convertIm(I2);
	w2 = I2.width(), h2 = I2.height();
	fact = min(1.0, min(double(w) / double(w2), double(h) / double(h2)));
	ox = 0.5 * (w - w2*fact), oy = 0.5 * (h - h2*fact);
	responses = Harris(I2, 0, R, offset);
	compute_NMS_and_display(W, 3, I2b, responses, "scale", responses0, nms0, anms0,
							w2, h2, R, offset, N, w, x0, y0, H, fact, ox, oy);

	// best Harris responses on viewpoint changed image
	cout << "Viewpoint" << endl;
	H = getRandomHomography(w, h);
	I2 = applyHomography(I, H, x0, y0);
	I2b = convertIm(I2);
	w2 = I2.width(), h2 = I2.height();
	fact = min(1.0, min(double(w) / double(w2), double(h) / double(h2)));
	ox = 0.5 * (w - w2*fact), oy = 0.5 * (h - h2*fact);
	responses = Harris(I2, 0, R, offset);
	cleanResponses(responses, w2, x0, y0, w, h, H);
	compute_NMS_and_display(W, 4, I2b, responses, "viewpoint", responses0, nms0, anms0,
							w2, h2, R, offset, N, w, x0, y0, H, fact, ox, oy);

	// Stop the programm when clicked
	endGraphics();

	return 0;
}