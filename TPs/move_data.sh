# Make directory
if [ ! -d "$2" ]; then
	mkdir "$2"
fi

# Move files
if ls $1/*.png 1> /dev/null 2>&1 ; then
	ls $1/*.png | xargs mv -t "$2"
fi
if ls $1/*.jpeg 1> /dev/null 2>&1 ; then
	ls $1/*.jpeg | xargs mv -t "$2"
fi
if ls $1/*_loc.txt 1> /dev/null 2>&1 ; then
	ls $1/*_loc.txt | xargs mv -t "$2"
fi
if [ -f $1/noise.txt ]; then
	mv $1/noise.txt -t $2
fi
if [ -f $1/original.txt ]; then
	mv $1/original.txt -t $2
fi
if [ -f $1/rotation.txt ]; then
	mv $1/rotation.txt -t $2
fi
if [ -f $1/scale.txt ]; then
	mv $1/scale.txt -t $2
fi
if [ -f $1/viewpoint.txt ]; then
	mv $1/viewpoint.txt -t $2
fi