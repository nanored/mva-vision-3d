\chapter{Géométrie épipôlaire}

\myminitoc

\paragraph{Rappel sur le produit vectoriel}
Le produit vectoriel de deux vecteurs est défini par :
$$ a \times b = [a]_\times b = \pmat{yz' - zy' \\ zx' - xz' \\ xy' - yx'} \qquad \text{Avec } [a]_x = \pmat{0 & -z & y \\ z & 0 & -x \\ -y & x & 0} $$
Ce produit est bilinéaire et antisymétrique. De plus il a un lien avec le déterminant puisque :
$$ a^\trans (b \times c) = \vmat{a & b & c} $$
On a aussi la propriété de composition suivante :
$$ (a \times b) \times c = (a^\trans c) b - (b^\trans c) a $$

\sect{Matrices essentielle et fondamentale}

\begin{center}
	\begin{tikzpicture}
		\draw[thick] (0, 0) node {$\bullet$} -- (0.8, 0.8) node {$\bullet$};
		\draw[thick] (1.4, 1.4) -- (4.5, 4.5);
		\draw (-0.1, 0.8) -- (-0.1, 2.9) -- (1.7, 1.1) -- (1.7, -1) -- cycle;
		\node[left] at (0, 0) {$C$};
		\node[above left] at (0.8, 0.8) {$\tilde{x}$};
		\node at (3.6, 3.6) {$\bullet$};
		\node at (4.2, 4.2) {$\bullet$};
		\node at (3.2, 3.2) {$\bullet$};
		\node[left=1mm] at (3.2, 3.2) {$X$ ?};
		\node[above left] at (3.6, 3.6) {$X$};
		\node[right=1mm] at (4.2, 4.2) {$X$ ?};
		
		\draw[dashed, thick] (5.4, 0) node {$\bullet$} -- (4.9, 1) node {$\bullet$};
		\draw[thick] (5.6, 1.85) node[right] {\scriptsize ligne épipolaire} -- (3.8, -0.34);
		\draw[dashed, thick] (4.5, 1.8) -- (3.6, 3.6);
		\draw (5.6, 0.8) -- (5.6, 2.9) -- (3.8, 1.1) -- (3.8, -1) -- cycle;
		\node[right] at (5.4, 0) {$C'$};
		\node[above left] at (4.9, 1) {$\tilde{x}'$};
		
		\draw (5.4, 0) -- (4.08, 0) node {$\bullet$};
		\draw (1.7, 0) -- (3.8, 0);
		\draw (0, 0) -- (1.42, 0) node {$\bullet$};
		\node[above] at (1.42, 0) {$e$};
		\node[above] at (4.08, 0) {$e'$};
		
		\draw[red, ->] (-1, 2) -- (-1, 1.5);
		\draw[red, ->] (-1, 2) -- (-0.65, 2.35);
		\draw[red, ->] (-1, 2) -- (-0.65, 1.65);
		\node[red, left] at (-1, 2) {$\mathcal{R}$};
		
		\draw[red, ->] (6.5, 1) -- (6.5, 0.5);
		\draw[red, ->] (6.5, 1) -- (6.15, 1.35);
		\draw[red, ->] (6.5, 1) -- (6.15, 0.65);
		\node[red, right] at (6.5, 1) {$\mathcal{R'}$};
	\end{tikzpicture}
\end{center}

\paragraph{}
On observe un point $X$ à partir de deux caméras de centres $C$ et $C'$. Les trois points $X$, $C$, et $C'$ définissent un plan qui contient les trois vecteurs $\vec{C\tilde{x}}$, $\vec{C'\tilde{x}'}$, et $\vec{T} = \vec{CC'}$. Pour simplifier on va se placer le repère de l'espace $\mathcal{R}$. On note $x$ les coordonnées de $\vec{C\tilde{x}}$, et $T$ les coordonnées de $\vec{T}$ dans ce repère. Finalement on note $x'$ les coordonnées de $\vec{C'\tilde{x}'}$ dans le repère $\mathcal{R'}$. En appelant $R$, la matrice de rotation pour passer du repère $\mathcal{R'}$ au repère $\mathcal{R}$, on a que les trois points $x$, $T$ et $Rx'$ forment une famille liée de $\R^3$. C'est à dire :
$$ \vmat{x & T & Rx'} = x^\trans \left( T \times Rx' \right) = x^\trans [T]_\times R x' = 0 $$
On défini alors la \textbf{matrice essentielle} par:
$$ E = [T]_\times R \qquad \text{tel que } x^\trans E x' = 0 $$
Cette matrice est intéressante mais elle traite des points de l'espace. Or on manipule des images et on a pas accès directement à $x$ et $x'$ mais plutôt aux coordonnées projectives $y = Kx$ et $y' = K'x'$ de ces deux points dans leurs images respectives. On défini alors la \textbf{matrice fondamentale} suivante :
$$ F = \left( K^{-1} \right)^\trans E K'^{-1} = \left( K^{-1} \right)^\trans [T]_\times R K'^{-1} \qquad \text{tel que } y^\trans F y' $$

\paragraph{Observations} 
\begin{itemize}
	\item Le point $e$ est l'\textbf{épipôle} de la première caméra. Ses coordonnées projectives pour la première caméra sont définies par :
	$$ e = KT \qquad \text{tel que } e^\trans F = 0 $$
	\item Le point $e'$ est l'épipôle de la seconde caméra. Ses coordonnées projectives pour la seconde caméra sont définies par :
	$$ e' = K' R^{-1} T \qquad \text{tel que } Fe' = 0 $$
	\item La ligne $Fx'$ définie la \textbf{ligne épipôlaire} de la première image. En effet si $x$ est un point de la première image résultant de la projection d'un point $X$ qui se projette aux coordonnées $x'$ sur la seconde image alors $x^\trans Fx' = 0$.
	\item La ligne $F^\trans x$ définie la ligne épipôlaire de la seconde image.
	\item Finalement on peut remarquer que si $T = 0$ alors $F = 0$. Sans déplacement on ne peut alors tirer aucune information de $F$.
\end{itemize}

\vspace{2mm}
\PROP{
	Une matrice $3 \times 3$ définie une matrice fondamentale si et seulement si son rang vaut 2.
}

\sect{Calculs des matrices}

\vspace{2mm}
\PROP[ (SVD)]{
	Soit $A$ une matrice de taille $m \times n$. Alors il existe deux matrices orthogonales $U$ et $V$ de tailles respectives $m \times m$ et $n \times n$, et une matrice diagonale $\Sigma$ de taille $m \times n$ tel que :
	$$ A = U \Sigma V^\trans $$
	\vspace{-7mm}
}

\dem
Dans l'idée voici les étapes de la démonstration: \\
On sait que $A^\trans A$ est symétrique semi définie positive. On peut donc l'orthogonalisé et ses valeurs propres sont positives. On écrit : $A^\trans A = V S^2 V^\trans$. Où $S$ est diagonale et ses valeurs propres nulles sont placées à la fin. \\
Pour tous les $i$ tel que $s_{i, i} \neq 0$ on pose $U_i = \frac{1}{s_{i, i}} V_i S$. Calculons le produit de deux colonnes $U_i$ et $U_j$ :
$$ U_i^\trans U_j = \frac{1}{s_{i, i} s_{j, j}} V_i^\trans A^\trans A V_j = \frac{1}{s_{i, i} s_{j, j}} S_{i, j}^2 = \delta_{i, j} $$
Ainsi en complétant $U$ par des vecteurs orthonormaux aux précédent on obtient une matrice $U$ orthogonale. On supprime ou ajoute des lignes à $S$ pour former une matrice $\Sigma$ aux bonne dimensions. \\
On peut alors vérifier que $A = U \Sigma V^\trans$. Mais je ne le ferai pas ici, car il faut un peu de temps et de rigueur.
\findem

\paragraph{Implémentation}
La preuve précédente décrit un algorithme pour obtenir cette décomposition mais le passage à la matrice $A^\trans A$ cause des instabilités numériques car on élève au carré les valeurs de la matrice. Il existe des solutions pour palier à ces problèmes numériques. Mais ces solutions sont d'une grande complexité. En pratique on utilisera des librairies comme des boîtes noirs.

\paragraph{Méthode des 8 points}
Comme $F$ est définie à un facteur multiplicatif près, 8 points suffisent à retrouver la matrice. Cela donne un problème linéaire simple. On peut bien évidement prendre plus de 8 points (et c'est ce qui est fait en pratique) et ensuite faire de l'optimisation. Pour chaque paire de point on a $\bx_i^\trans F \bx_i' = 0$. On transforme alors $F$ en un vecteur $f$ pour obtenir un système linéaire :
$$ \bx_i^\trans F \bx_i' = 0 \Leftrightarrow A_i^\trans f = 0 \quad \text{Avec } f = \pmat{f_{11} & f_{12} & \cdots & f_{33}}^\trans $$
$$ A_i^\trans = \pmat{x_i x_i' & x_i y_i' & x_i & y_i x_i' & y_i y_i' & y_i & x_i' & y_i' & 1} $$
On impose finalement la contrainte $\| f \| = 1$ en dernière contrainte. Voici alors la tête de notre problème d'optimisation pour $n$ points :
$$ \min_f \| A f \|^2 \quad \text{t.q. } \| f \| = 1 \quad \text{Avec } A = \pmat{A_1 & \cdots & A_n}^\trans $$
La solution de ce problème obtenue pour $f$ un vecteur propre de $A^\trans A$ associée à la plus petite valeur propre (c'est à dire $f = V_9$ dans la SVD de $A$). Finalement on obtient généralement une matrice $F$ inversible alors que le rang doit être 2. Pour palier à ça une solution consiste à calculer la SVD de $F$ et mettre $\sigma_3$ à zéro puis de recomposer $F$.

\paragraph{La méthode des 7 points}
Forcer la contrainte $\det F = 0$ après minimisation n'est pas optimal. On peut alors imposer cette condition au cours de la minimisation ce qui conduit à la méthode des 7 points puisqu'une contrainte est ajoutée. Le système obtenue est alors un système linéaire $A f = 0$ avec $A$ de taille $7 \times 9$. On décompose alors $A$ en SVD afin d'obtenir deux vecteurs $f_1$ et $f_2$ qui forment la base du noyau de $A$. On recherche ensuite une solution de la forme $f = f_1 + \lambda f_2$ avec $\det F = 0$. Cela revient alors à chercher les zéros du polynôme suivant :
$$ P(X) = \det \left( F_1 + X F_2 \right) $$
C'est un polynôme de degré 3 qui admet alors un ou trois zéros. \\
Cette méthode nécessite une paire de points en moins ce qui diminue les chances d'avoir une mauvaise correspondance de points. Mais vient quand même la question de savoir comment peut-on être sûr de ne pas incorporer de mauvaises correspondances dans nos équations.

\paragraph{Normalisation}
L'échelle des coefficients de $A$ peut poser pas mal de problèmes numériques. En effet si les $x_i$ et $y_i$ sont de l'ordre de $10^3$ alors la matrice $A$ contient des coefficients de l'ordre de $10^6$ et de l'ordre de 1 (pour $A_{i,9}$). C'est pourquoi il est intéressant de normaliser les coordonnées avec une matrice de la forme :
$$ N = \pmat{10^{-3} \\ & 10^{-3} \\ && 1} \qquad \bx_i \gets N \bx_i, \; \bx_i \gets N \bx_i $$

\paragraph{Calcul de E}
$E$ dépend de 5 paramètres. En effet il y en a 3 pour la rotation $R$, puis 3 pour la translation $T$ et finalement une de moins car la matrice est définie à un facteur de multiplication près. Le calcul de cette matrice est bien plus complexe que $F$. Il peut être montrer que $E$ est une matrice essentielle si et seulement si $E$ possède une valeur singulière nulle et deux autre positives. Cela se traduit par :
$$ 2 E E^\trans E - \tr(EE^\trans) E = 0 \quad \text{et} \quad \det E = 0 $$
La méthode des 5 points a été développé en 2004 par Nister. Le noyau de $A$ est alors de dimension $4$. Et les contraintes donnent 10 équations de degré 3 pour trouver quel est la bonne matrice dans le noyau de $A$. Bref c'est compliqué ...

\sect{RANSAC}

Comme évoqué précédemment on peut avoir des paires de points qui ne correspondent pas. Une estimation robuste de la matrice doit faire abstraction des valeurs aberrantes. Une idée consiste à tester plusieurs modèles construits à partir de différents sous-ensembles de paires de points. C'est algorithme est l'algorithme \textbf{RANSAC pour RANdom SAmple Consensus}.

\begin{center}
	\begin{algorithm}
		\caption{RANSAC}
		\KwIn{Un ensemble de $n$ échantillons et une précision $\sigma$ à atteindre}
		\vspace{1mm}
		\Repeat{Un modèle satisfaisant à été trouvé}{
			1. Sélectionner $k$ échantillons parmi les $n$, où $k$ est le nombre minimal pour construire un modèle. \\
			2. Calculer un modèle avec ces échantillons et compter le nombre d'échantillons parmi les $n$ qui sont correctement estimés à une précision $\sigma$ \\
			3. Si ce nombre est plus grand que pour tous les modèles précédemment rencontrés alors le garder
		}
	\end{algorithm}
\end{center}

Dans notre cas la précision est donné par les distances $d(\bx_i', F^\trans \bx_i)$ des points de la seconde image à leur ligne épipôlaire prédite par le modèle grâce aux points correspondant dans la première image.
$$ d(\bx_i', F^\trans \bx_i) = \dfrac{\left| \left( F^\trans \bx_i \right)_1 x_i' + \left( F^\trans \bx_i \right)_2 y_i' + \left( F^\trans \bx_i \right)_3 \right|}{\sqrt{\left( F^\trans \bx_i \right)_1^2 + \left( F^\trans \bx_i \right)_2^2}} $$
Supposons que nous avons $m$ paires de points corrects. Alors la probabilité de prendre $k$ paires de points corrects est $(m / n)^k$. On veut que la probabilité de $N_{iter}$ itérations de RANSAC donnent que des sous-ensembles contenant une mauvaise paire de points soit inférieur à $\beta = 1\%$ :
$$ \left( 1 - (m / n)^k \right)^{N_{iter}} \leqslant \beta \qquad \Leftrightarrow \qquad N_{iter} \geqslant \dfrac{\log \beta}{\log \left( 1 - (m / n)^k \right)} $$
$m$ est inconnu mais on connaît une borne inférieur qui est le plus grand nombre de paires de points expliqué par un modèle obtenu jusque là. A chaque fois qu'on trouve un meilleur modèle on peut alors recalculer $m$.

\sect{Rectification épipôlaire}

\paragraph{}
Il est commode d'avoir les lignes épipôlaires parallèles et à la même ordonné dans les deux images. La conséquence est que les épipôles sont les points à l'infini horizontalement.
$$ e = e' = \pmat{1 \\ 0 \\ 0} $$
On peut toujours se retrouver dans cette situation en appliquant une rotation virtuelle à la caméra qui résulte en une homographie. \\
De plus deux point $\bx$ et $\bx'$ peuvent être des projection du même point $X$ de l'espace sur la première et la seconde caméra si et seulement si ces deux point ont la même ordonnée. En effet la ligne épipôlaire d'un point est la ligne horizontale de même ordonnée. Cela donne :
$$ \bx^\trans F \bx' = 0 \Leftrightarrow y - y' = 0 \Leftrightarrow \pmat{x & y & 1} \pmat{0 \\ 1 \\ -y'} = 0 \Leftrightarrow \bx^\trans \pmat{0 & 0 & 0 \\ 0 & 0 & 1 \\ 0 & -1 & 0} \bx' = 0 $$
Finalement il existe une constante multiplicative $\lambda$ tel que :
$$ F = \lambda \bmat{1 \\ 0 \\ 0}_\times $$
Enfin les épipôles sont à l'infini si et seulement si les plans images des caméras sont confondus. Ainsi il n'y a pas de rotation entre les repères des deux caméras ($R = I_3$) :
$$ P = K \pmat{I_3 & 0} \qquad P' = K' \pmat{I_3 & T} \qquad \text{Avec } T = \pmat{x & y & z}^\trans $$
$$ K = \pmat{f_x & s & c_x \\ 0 & f_y & c_y \\ 0 & 0 & 1} \qquad K = \pmat{f'_x & s' & c'_x \\ 0 & f'_y & c'_y \\ 0 & 0 & 1} $$
Pour le calcul de $F = \left( K^{-1} \right)^\trans [T]_\times K'^{-1}$ on a besoin des trois matrices suivante :
$$ \left( K^{-1} \right)^\trans = \dfrac{1}{f_x f_y} \pmat{f_y & 0 & 0 \\ -s & f_x & 0 \\ s c_y - f_y c_x & -f_x c_y & f_x f_y} \qquad K'^{-1} = \dfrac{1}{f'_x f'_y} \pmat{f'_y & -s' & s' c'_y - f'_y c'_x \\ 0 & f'_x & -f'_x c'_y \\ 0 & 0 & f'_x f'_y} $$
$$ [T]_\times = \pmat{0 & -z & y \\ z & 0 & -x \\ -y & x & 0} $$
D'après la forme de $F$ trouvée précédemment on sait que la première ligne de $F$ est nulle. Comme les distances focales sont nulles on obtient alors que la première ligne de $[T]_\times K'^{-1}$ est nulle :
$$ \syst{lll}{
	-z f'_x & = & 0 \\
	f'_x \left( z c'_y + y f'_y \right) & = & 0
} \Rightarrow z = y = 0 $$
Ainsi il existe $B$ tel que $T = Be_1$. Cela simplifie grandement les calculs qui aboutissent à :
$$ F = \dfrac{B}{f_y f'_y} \pmat{0 & 0 & 0 \\ 0 & 0 & -f'_y \\ 0 & f_y & c_y f'_y - c'_y f_y} $$
Ainsi on doit avoir :
$$ f'_y = f_y \quad c'_y = c_y $$
On cherche alors des homographies $H$ et $H'$ tel que $F = H^\trans [e_1]_\times H'$. Appliquer les inverses de ces homographies aux deux images permettrait de rectifier les lignes épipôlaires. Beaucoup de techniques pour faire ça, mais on ne les verra pas ici. 