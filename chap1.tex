\chapter{Caméra et Projection}

\myminitoc

\sect{Caméra sténopé (\textit{pinhole})}

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.4]{pinhole.png}
	\qquad
	\begin{tikzpicture}
	\node at (0, 0) {\includegraphics[scale=0.4]{cam_model.png}};
	\node at (2.1, 2.6) {$P$};
	\node at (1.6, 1.1) {$p$};
	\node at (-1.8, -1.4) {$C$};
	\node[red] at (0.4, -1.8) {$x$};
	\node[greenTikz] at (-0.8, -2.6) {$y$};
	\node[blue] at (-1.1, -0.4) {$z$};
	\end{tikzpicture}
\end{figure}

\paragraph{}
On se déplace dans le modèle idéal où l'ouverture de la caméra est réduite à un point. On ignore toute distorsion géométrique et tout flou qui pourrait par exemple être causé par la diffraction. Afin de ne pas avoir une image retournée comme dans les caméras physiques on place le plan de projection devant le point $C$ de la caméra. La direction $\vec{z}$ est la direction dans laquelle regarde la caméra. Les deux autres axes sont parallèles aux bordures de notre de projection.

\paragraph{}
Le point de projection $p$ se trouve sur la droite passant par $C$ et $P$. On écrit :
$$ \vec{Cp} = \lambda \vec{CP} $$
Le plan de projection est placé à la profondeur $f$ correspondant à la focale. On a donc :
$$ \pmat{x \\ y \\ f} = \lambda \pmat{X \\ Y \\ Z} $$
On en déduit alors l'égalité $\lambda = f / Z$. Par la suite on travaille avec une grille de pixels située sur le plan de projection. On note alors $c = (c_x, c_y)$ les coordonnées de la projection de la droite $C\vec{z}$ sur cette grille. On introduit aussi un facteur d'échelle $\alpha$ et on obtient :
$$ \pmat{u \\ v} = \pmat{\alpha x + c_x \\ \alpha y + c_y} = \pmat{(\alpha f) X / Z + c_x \\ (\alpha f) Y / Z + c_y} $$

\sect{Géométrie projective}

\paragraph{}
Comme deux points alignés avec $C$ ont la même projection on défini naturellement la relation d'équivalence $\mathcal{R}$ suivante :
$$ x \mathcal{R} y \; \Leftrightarrow \; \exists \lambda \neq 0, \, x = \lambda y $$
Cette relation permet alors de définir le plan projectif :
$$ \Pp^2 = \left( \R^3 \setminus \{ 0 \} \right) / \mathcal{R} $$
On représentera souvent les vecteurs de $\Pp^2$ par des vecteurs à trois dimensions dont la troisième composante vaut 1. Les points de notre projections qui ne peuvent pas s'exprimer de la sorte sont des points situés à l'infini. Ils ont une troisième composante nulle.
$$ \pmat{x & y & \epsilon} = \pmat{x/\epsilon & y/\epsilon & 1} \xrightarrow[\epsilon \rightarrow 0]{} \pmat{x & y & 0} $$

\subs{Lignes et intersections}

\paragraph{}
Un plan de $\R^3$ passant par $0$ à un projection qui est une ligne dans $\Pp^2$. Un plan dans $\R^3$ et donc une ligne dans $\Pp^2$ est représenté par son vecteur orthogonal $(a, b, c)$ qui vérifie l'équation :
$$ \pmat{a & b & c}^\trans \pmat{X & Y & Z} = 0 $$

\paragraph{}
La ligne passant par les deux points $x_1$ et $x_2$ est définie par:
$$ l = x_1 \times x_2 \qquad \text{en effet } \, \left( x_1 \times x_2 \right)^\trans x_i = \begin{vmatrix} x_1 & x_2 & x_i \end{vmatrix} = 0 $$
L'intersection des deux lignes $l_1$ et $l_2$ est définie par:
$$ x = l_1 \times l_2 \qquad \text{en effet } \, l_i^\trans \left( l_1 \times l_2 \right) = \begin{vmatrix} l_i & l_1 & l_2 \end{vmatrix} = 0 $$
Les points à l'infini forment une ligne à l'infini représentée par :
$$ l_\infty = \pmat{0 \\ 0 \\ 1} \qquad \text{en effet } \, l_\infty^\trans \pmat{x \\ y \\ 0} = 0 $$
Autre point intéressant : les lignes "parallèles" s'intersectent à l'infini :
$$ \pmat{a \\ b \\ c_1} \times \pmat{a \\ b \\ c_2} = (c2 - c_1) \pmat{b \\ -a \\ 0} \in l_\infty $$

\subs{Matrice de calibration}

\paragraph{}
Pour simplifier la suite, on remplace $\alpha f$ par $f$. On a alors :
$$ \pmat{u \\ v} = \dfrac{1}{Z} \pmat{f X + c_x Z \\ f Y + c_y Z} \quad \Leftrightarrow \quad p = Z \pmat{u \\ v \\ 1} = \pmat{f & & c_x \\ & f & c_y \\ & & 1} \pmat{X \\ Y \\ Z} $$
La \textbf{matrice de calibration} est alors la matrice $K$ qui apparaît ci-dessous.
$$ K = \pmat{f & & c_x \\ & f & c_y \\ & & 1} $$
Si les pixels ne sont pas des rectangles mais des parallélogrammes alors on obtient une matrice de calibration différente :
\begin{center}
	\begin{tikzpicture}[scale=0.5]
	\draw[->] (0, 0) -- (5, 0);
	\draw[->] (0, 0) -- (0, -4);
	\draw[red, thick] (0, 0) -- node[black, above] {\footnotesize $f_x$} (3, 0) -- (4, -3) -- (1, -3) -- cycle;
	\draw[dashed] (0, -3) -- (1, -3);
	\node[left] at (0, -1.5) {\footnotesize $f_y$};
	\draw[] (0.8, 0) arc(0:-72:0.8) node[below right=-1mm, pos=0.5] {\footnotesize $\theta$};
	\node at (14, -1.75) {$\displaystyle K = \pmat{f_x & s & c_x \\ & f_y & c_y \\ & & 1}$ \quad avec $\displaystyle s = -f_x \cotan \theta$};
	\end{tikzpicture}
\end{center}

\paragraph{}
La caméra pouvant être translaté on introduit les matrices de translation qui nécessite de passer à la 4ème dimension :
$$ \pmat{X + t_x \\ Y + t_y \\ Z + t_z} = \pmat{1 & & & t_x \\ & 1 & & t_y \\ & & 1 & t_z} \pmat{X \\ Y \\ Z \\ 1} = T P $$
La caméra peut aussi subir une rotation. Les rotations sont représentées par les matrices orthogonales de déterminant 1. On notera $R$ les matrices de rotations.
La \textbf{matrice de projection} est alors le produit d'une translation suivie d'une rotation suivie de la matrice de calibration :
$$ P = K \pmat{R & T} $$

\PROP{
	SI $P$ est une matrice $3 \times 4$ dont la sous-matrice $3 \times 3$ de gauche est inversible alors il existe une unique décomposition :
	$$ P = K \pmat{R & T} $$
	\vspace{-7mm}
}

\dem
On pose $P = \pmat{A & P_4}$. Appliquer l'orthonormalisation de Gram-Schmidt sur les lignes de A conduit à ce qu'on appelle la décomposition RQ où R signifie une matrice triangulaire supérieure et Q une matrice orthogonale. Cela nous donne donc $A = K R$. On se retrouve alors avec $T = K^{-1} P_4$.
\findem

\subs{Perspective}
On s'intéresse à toutes les droites parallèles de direction $d$ dans l'espace. Considérons la droite de direction $d$ et passant par $P$. Alors :
$$ K(P + \lambda d) = K P + \lambda K d $$
Ainsi la projection de la droite est représentée par $l_P = (KP) \times (Kd)$. On pose alors le point $v = Kd$. Ainsi pour tout $Q \in R^3$ on a $l_Q^\trans v = 0$. Cela veut dire que toutes les droites de directions $d$ s'intersecte en un seul point $v$. Ce point est appelé un \textbf{point de fuite}.

\paragraph{}
Si on considère deux direction $d_1$ et $d_2$. On obtient deux points de fuites $v_1 = Kd_1$ et $v_2 = Kd_2$. Alors le point de fuite des lignes de directions $\alpha d_1 + \beta d_2$ est $\alpha v_1 + \beta v_2$ qui appartient à la ligne $v_1 \times v_2$. Si $d_1$ et $d_2$ sont des directions horizontales alors la ligne $v_1 \times v_2$ est appelée \textbf{ligne de fuite}. Cette ligne correspond à l'horizon.

\sect{Homographies}

\paragraph{Rotation de caméra}
Une scène peut être prise en photos sous plusieurs angles/positions. Un point de la scène peut alors avoir les coordonnées $\bx$ dans une photos et les coordonnées $\bx'$ dans une seconde. Par exemple si on fait simplement une rotation de la caméra on obtient la relation suivante :
$$ \bx' = K' R K^{-1} \bx = H \bx $$
La matrice $K^{-1}$ permet de retourner dans les coordonnées de l'espace. La matrice $R$ fait une rotation dans l'espace puis on reprojette notre point qui a subit une rotation avec la matrice $K'$ qui peut être différente de $K$ si entre temps les paramètre de l'appareil d'acquisition ont changé.

\paragraph{Photo d'un plan}
Une homographie n'est pas nécessaire une transformation d'une projection à une autre, mais plus généralement d'un plan à un autre. Par exemple si on observe le plan $Z = 0$ alors $P$ est de la forme $\pmat{X & Y & 0 & 1}^\trans$ (la quatrième coordonnée est ajoutée pour les translation). $P$ peut alors être contracté en $\bx = \pmat{X & Y & 1}^\trans$. On peut alors obtenir les coordonnées du point $P$ sur une photo prise de n'importe qu'elle position dans l'espace avec n'importe quelle rotation :
$$ \bx' = K \pmat{R_1 \, R_2 \, R_3 & T} P = K \pmat{R_1 \, R_2 & T} \bx = H \bx $$

\paragraph{}
Les deux cas précédent font apparaître une matrice $H$ qui réalise une transformation linéaire des coordonnées dans un plan vers les coordonnées dans un autre plan. Une telle transformation est appelée une \textbf{homographie}. Avec :
$$ \bx = \pmat{x \\ y \\ 1} \qquad \bx' = \lambda \pmat{x' \\ y' \\ 1} \qquad H = \pmat{h_1^\trans \\[1mm] h_2^\trans \\[1mm] h_3^\trans} $$
On obtient le système :
$$ \left\{ \begin{array}{rll}
\lambda x' & = & h_1^\trans \bx \\
\lambda y' & = & h_2^\trans \bx \\
\lambda & = & h_3^\trans \bx
\end{array} \right. \Rightarrow \left\{ \begin{array}{rll}
x' & = & \dfrac{h_1^\trans \bx}{h_3^\trans \bx} = \dfrac{h_{1,1}x + h_{1,2}y + h_{1,3}}{h_{3,1}x + h_{3,2}y + h_{3,3}} \\[5mm]
y' & = & \dfrac{h_2^\trans \bx}{h_3^\trans \bx} = \dfrac{h_{2,1}x + h_{2,2}y + h_{2,3}}{h_{3,1}x + h_{3,2}y + h_{3,3}}
\end{array} \right. $$
On remarque finalement que $\gamma H$ résulte en la même transformation que $H$ si $\gamma \in \R^*$. On peut alors poser $h_{3, 3} = 1$. L'espace des homographies est alors de dimension 8 et la données de 4 paires de points $(\bx, \bx')$ avec $\bx' = H \bx$ permet de retrouver l'homographie grâce aux équations :
\begin{equation}
	\left\{ \begin{array}{rll}
	x' & = & \dfrac{h_{1,1}x + h_{1,2}y + h_{1,3}}{h_{3,1}x + h_{3,2}y + 1} \\[4mm]
	y' & = & \dfrac{h_{2,1}x + h_{2,2}y + h_{2,3}}{h_{3,1}x + h_{3,2}y + 1}
	\end{array} \right.
	\label{eq:homo}
\end{equation}

\paragraph{}
Le théorème suivant est une généralisation de l'unicité d'une homographie étant donné un certain nombre de points.

\PROP[ (Unicité d'homographie)]{
	Étant donné les vecteurs $e_1, ..., e_{d+1}$ et $f_1, ..., f_{d+1}$ dans $\R^d$ tels que pour tout $i \in \{1, ..., d+1\}$, les familles $\{ e_j \}_{j \neq i}$ et $\{ f_j \}_{j \neq i}$ sont des bases de $\R^d$. Alors il existe un unique isomorphisme $H$ (à un facteur multiplicatif près) et des scalaires uniques $\lambda_i$ tel que pour tout $i$, $H e_i = \lambda_i f_i$. \\
	Ainsi étant donné $(n+2)$ paires $(x_i, x'_i) \in \Pp^n$ il existe une unique homographie associant les $x'_i$ aux $x_i$.
}

\dem
$(e_1, ..., e_d)$ et $(f_1, ..., f_d)$ sont des base de $\R^d$. On peut donc écrire de manière unique les vecteurs $e_{d+1}$ et $f_{d+1}$ dans ces bases :
$$ e_{d+1} = \sum_{i = 1}^d \mu_i e_i \qquad f_{d+1} = \sum_{i = 1}^d \nu_i f_i $$
On obtient donc :
$$ \sum_{i=1}^d \nu_i \lambda_{d+1} f_i = \lambda_{d+1} f_{d+1} = H e_{d+1} = \sum_{i = 1}^d \mu_i H e_i = \sum_{i = 1}^d \mu_i \lambda_i f_i $$
D'où les égalités :
$$ \forall i = 1, ..., d ~: \quad \mu_i \lambda_i = \nu_i \lambda_{d+1} $$
Or si $\mu_i \neq 0$ alors la famille $\{ e_j \}_{j \neq i}$ est linéairement dépendante. C'est une contradiction avec nos hypothèses donc tous les $\mu_i$ et $\nu_i$ sont non nuls. Cela nous donne donc l'égalité :
$$ \forall i = 1, ..., d ~: \quad \lambda_i = \frac{\nu_i}{\mu_i} \lambda_{d+1} $$
On peut alors fixer $\lambda_{d+1} = 1$ comme l'unicité est à un facteur multiplicatif près. Et alors il existe une unique matrice $H$ qui envoie la base $\{ e_i \}_{1 \leqslant i \leqslant d}$ sur la base $\{ \lambda_i f_i \}_{1 \leqslant i \leqslant d}$.
\findem

\paragraph{Panorama}
On se donne un ensemble de $n$ paires de points $(\bx_i, \bx'_i)_{1 \leqslant i \leqslant n}$ sur deux photos d'une même scène. On suppose que la transformation des $x_i$ vers les $x'_i$ est une homographie. Par exemple la caméra a subi une rotation ou bien on observe un plan. On reprend alors l'équation \refeq{eq:homo}.
$$ \left\{ \begin{array}{rll}
x'_i & = & h_{1,1}x + h_{1,2}y + h_{1,3} - h_{3,1} x_i x'_i - h_{3,2} y_i x'_i \\[4mm]
y'_i & = & h_{2,1}x + h_{2,2}y + h_{2,3} - h_{3,1} x_i y'_i + h_{3,2} y_i y'_i
\end{array} \right. $$
On a donc un système linéaire : $A_i h = \bx'_i$. Où :
$$ h = \pmat{h_{1,1} & h_{1,2} & \dots & h_{3,2}} \qquad A_i = \pmat{x_i & y_i & 1 & 0 & 0 & 0 & -x_i x'_i & -y_i x'_i \\ 0 & 0 & 0 & x_i & y_i & 1 & -x_i y'_i & -y_i y'_i} $$
Cela donne finalement le système : $A h = \bx'$. Avec :
$$ A = \pmat{A_1 \\ \vdots \\ A_n} \qquad \bx' = \pmat{\bx'_1 \\ \vdots \\ \bx'_n} $$
Lorsque $n$ est strictement plus grand que 4, on se retrouve avec un problème d'optimisation car il est possible qu'aucun vecteur $h$ ne vérifie le système. Voici alors plusieurs fonctions possibles à optimiser :
\begin{itemize}
	\item L'erreur quadratique algébrique (qui n'a pas de sens géométrique) :
	$$ \min_h \| Ah - \bx' \|^2 $$
	\item Une autre erreur qui a plus de sens géométrique est l'erreur de transfert :
	\begin{center}
		\vspace{3mm}
		\begin{tikzpicture}
			\draw (0, 0) rectangle (3, 2.2);
			\node at (0.8, 1.85) {\small Image 1};
			\draw (4, 0) rectangle (7, 2.2);
			\node at (6.2, 1.85) {\small Image 2};
			
			\draw[greenTikz, dashed, thick] (1.1, 1.1) -- node[right, pos=0.3] {$d$} (1.5, 0.5);
			\draw[greenTikz, dashed, thick] (5.4, 1) -- node[below left, pos=0.5] {$d'$} (4.7, 1.5);
			
			\node (x) at (1.1, 1.1) {\footnotesize $\times$};
			\node[above left] at (x) {\footnotesize$\bx$};
			\node (x2) at (5.4, 1) {\footnotesize $\times$};
			\node[below right] at (x2) {\footnotesize $\bx'$};
			\node[red] (hx) at (4.7, 1.5) {\footnotesize $\bullet$};
			\node[above right, red] at (hx) {\footnotesize $H \bx$};
			\node[red] (hx2) at (1.5, 0.5) {\footnotesize $\bullet$};
			\node[below left=-0.5mm, red] at (hx2) {\footnotesize $H^{-1} \bx'$};
			
			\draw[blue, ->] (x) edge[bend left] node[below, pos=0.4] {\footnotesize $H$} (hx);
			\draw[blue, ->] (x2) edge[bend left] node[above, pos=0.5] {\footnotesize $H^{-1}$} (hx2);
		\end{tikzpicture}
		\vspace{1mm}
	\end{center}
	L'erreur de transfert est alors : $ \min_H d'^2 = d(\bx', H \bx)^2 $. \\
	Cette erreur peut-être symétrisé par :
	$$ \min_H \; d^2 + d'^2 = d(\bx, H^{-1} \bx')^2 + d(\bx', H \bx)^2 $$
	\item On peut aussi supposer que nos points $\bx$ et $\bx'$ sont des observations bruités. On peut alors rajouter de nouveaux paramètres à optimiser qui seraient la vrai position $\hat{\bx}$.
	\begin{center}
		\vspace{3mm}
		\begin{tikzpicture}
		\draw (0, 0) rectangle (3, 2.2);
		\node at (0.8, 1.85) {\small Image 1};
		\draw (4, 0) rectangle (7, 2.2);
		\node at (6.2, 1.85) {\small Image 2};
		
		\draw[greenTikz, dashed, thick] (1.1, 1.1) -- node[right, pos=0.3] {$d$} (1.5, 0.5);
		\draw[greenTikz, dashed, thick] (5.4, 1) -- node[below left, pos=0.5] {$d'$} (4.7, 1.5);
		
		\node (x) at (1.1, 1.1) {\footnotesize $\times$};
		\node[above left] at (x) {\footnotesize$\bx$};
		\node (x2) at (5.4, 1) {\footnotesize $\times$};
		\node[below right] at (x2) {\footnotesize $\bx'$};
		\node[red] (hx) at (4.7, 1.5) {\footnotesize $\bullet$};
		\node[above right, red] at (hx) {\footnotesize $H \hat{\bx}$};
		\node[red] (hx2) at (1.5, 0.5) {\footnotesize $\bullet$};
		\node[below left=-0.5mm, red] at (hx2) {\footnotesize $\hat{\bx}$};
		
		\draw[blue, ->] (x) edge[bend left] node[below, pos=0.4] {\footnotesize $H$} (hx);
		\end{tikzpicture}
		\vspace{1mm}
	\end{center}
	Ce qui nous donne :
	$$ \min_{H, \hat{\bx}} \; d(\bx, \hat{\bx})^2 + d(\bx', H \hat{\bx})^2 $$
\end{itemize}