\chapter{Coupes de graphes}

\myminitoc

\paragraph{Rappel}
Le cours a commencé avec quelques rappels sur MAX-FLOW et MIN-CUT. Je ne ferai pas les rappels dans ces notes de cours. En revanche une slide contenait une généralisation du problème MIN-CUT appelé MIN-$k$-CUT, que je décrit ici. On se donne $k$ terminaux $s_1, ..., s_k$. Le but est alors de partitionner le graphe en $k$ partitions contant chacune exactement un terminal. L'objectif est d'avoir une coupe minimal. En notant $V_1, ..., V_k$ notre partition des sommets le problème s'écrit :
$$ \min \sum_{i = 1}^k \sum_{j = i+1}^k \sum_{p \in V_i} \sum_{q \in V_j} c(p, q) $$
Pour $k \geqslant 3$ ce problème est NP-difficile et même APX-difficile c'est où APX est la classe de complexité des problèmes approximables en temps polynomial. En revanche la restriction aux graphes planaires admet des solutions exactes.

\sect{Restauration d'images noir et blanc}

\begin{center}
	\begin{tikzpicture}
		\node[draw] (a) at (0, 0) {\includegraphics[scale=8]{bw0.png}};
		\node[draw] (b) at (3.5, 0) {\includegraphics[scale=8]{bw1.png}};
		\draw[->, red] (a) -- node[above] {bruit} (b);
		\node[draw] (c) at (7, 0) {\includegraphics[scale=8]{bw2.png}};
		\draw[->] (b) -- node[above] {seuil} (c);
		\node[draw] (d) at (10.5, 0) {\includegraphics[scale=8]{bw3.png}};
		\draw[->, blue] (c) -- node[above] {coupe} (d);
	\end{tikzpicture}
\end{center}

\paragraph{}
Pour chaque pixel $p \in \mathcal{P}$ on va assigner un label $l \in \{0, 1\}$ pour dire si sa couleur est blanche ou noir. On note $D_p(l)$ la pénalité obtenu en assignant le label $l$ à $p$. On associe le label 0 à la source $s$ de notre graphe et le label 1 au puits $t$. Ainsi on défini les poids des arrêtes terminales par :
$$ w(s, p) = D_p(1) \qquad w(p, t) = D_p(0) $$
La nouveau label d'un pixel sera alors 0 s'il est dans la composante de la source $s$ et 1 s'il est dans la composante du puits $t$. On note $I_p$ le label associé à $p$ dans notre image de départ (celle obtenue après seuillage et avant la coupe sur le schéma ci-dessus). Une façon classique de définir les pénalité est la suivante :
$$ D_p(l) = \syst{ll}{0 & \text{Si } I_p = l \\ 1 & \text{Si } I_p \neq l} $$
Finalement on veut minimiser la longueur des contours. On pose alors une valeurs $\lambda > 0$ tel que :
$$ \forall p, q \in \mathcal{P}, \, w(p, q) = \lambda $$
Ainsi en cherchant la coupe minimal $C = \left\{ \mathcal{S}, \mathcal{T} \right\}$ de notre graphe on cherche à minimiser :
$$ \min \; | C | \; = \; \sum_{p \in \mathcal{P}} D_p \left( \mathbbm{1}_\mathcal{T}(p) \right) \; + \; \lambda \sum_{p, q \in \mathcal{N}} \mathbbm{1}_{\mathcal{S} \times \mathcal{T}}(p, q) $$
Où $\mathcal{N}$ est l'ensemble des couples de voisins.
Si $\lambda$ est très petit alors la coupe ne modifie pas les labels. En revanche si $\lambda$ est trop grand on obtient soit une image toute blanche soit une image toute noire. Il faut donc trouver un bon équilibre.

\paragraph{}
De manière plus générale on note $f_p$ le label de $p$ :
$$ f_p = \syst{ll}{1 & \text{Si } p \in \mathcal{T} \\ 0 & \text{Si } p \in \mathcal{S}} $$
L'énergie de $f$ que l'on veut minimiser s'exprime alors :
$$ E(f) = E_{data}(f) + E_{regul}(f) = \sum_{p \in \mathcal{P}} D_p(f_p) \; + \; \sum_{p, q \in \mathcal{N}} V_{p, q} (f_p, f_q) $$

\PROP[ (Kolmogorov \& Zabih 2004)]{
	Si les potentiels $V$ vérifient la condition de régularité suivante :
	$$ V_{p, q}(0, 0) + V_{p, q}(1, 1) \leqslant V_{p, q}(0, 1) + V_{p, q}(1, 0) $$
	Alors il existe un graphe dont la coupe minimale définie une labellisation $f$ qui atteint le minimum d'énergie.
}

\sect{Segmentation d'images}

\begin{center}
	\begin{tikzpicture}
		\node at (0, 0) {\includegraphics[height=3.5cm]{fish0.jpg}};
		\node at (6, 0) {\includegraphics[height=3.5cm]{fish1.png}};
		\draw[blue, very thick] (-1, 0.35) rectangle (-0.85, 0.5);
		\draw[blue, very thick] (-1.8, 0.4) rectangle (-1.65, 0.25);
		\node[blue] at (-1.33, 0.6) {$\mathbb{\mathcal{O}}$};
		\draw[red, very thick] (0.5, 1) rectangle (0.7, 0.8);
		\draw[red, very thick] (1.5, 0.35) rectangle (1.3, 0.15);
		\draw[red, very thick] (0, -0.75) rectangle (-0.2, -0.95);
		\node[red] at (1.3, 0.8) {$\mathbb{\mathcal{B}}$};
	\end{tikzpicture}
\end{center}

\paragraph{}
Étant donné une image et des exemples de pixels d'un objet et du fond (\textit{background}) $(\mathcal{O}, \mathcal{B})$ le but est de séparé tous les pixels de l'image dans ces deux classes. Il faut donc réfléchir à des critères qui vont nous permettre de faire cette séparation. On peut donc penser aux caractéristiques suivantes :
\begin{itemize}
	\item Les pixels de l'objet et du fond ressemblent à ceux donnés en exemple dans le couple $(\mathcal{O}, \mathcal{B})$.
	\item Les contours de la segmentation se trouvent sur les zones de fort gradient. De plus afin d'éviter les fractales on veut que ces contours soient courts.
\end{itemize}
On va maintenant construire une énergie $E$ à minimiser. On pose :
$$ E(f) = D(f) + \lambda R(f) $$
Où $D$ est le terme de données qui pénalise les pixels qui ne sont pas assignés à un label qui correspond à l'image de départ et $R(f)$ est le terme de régularisation qui pénalise les discontinuité dans les voisinages de pixels. Une façon de voire cette décomposition est avec la formulation bayésienne suivante. Minimiser $E$ revient à maximiser la probabilité à posteriori $\Pp(f \mid I)$. Avec la formule de Bayes on obtient :
$$ {\color{red} \underbrace{- \log \Pp(f \mid I) - \log \Pp(I)}_{E(f) + c^{te}}} = {\color{blue} \underbrace{- \log \Pp(I \mid f) }_{D(f)}} \; {\color{green} \underbrace{- \log \Pp(f) }_{R(f)}} $$

\paragraph{Terme des données / Vraisemblance}
En supposant les pixels indépendants, on a :
$$ \Pp(I \mid f) = \prod_{p \in \mathcal{P}} \Pp(I_p \mid f_p) $$
D'où :
$$ D(f) = \sum_{p \in \mathcal{P}} D_p(f_p) \qquad \text{Avec } \; D_p(f_p) = - \log \Pp(I_p \mid f_p) $$
Voici alors différente approche pour définir la vraisemblance :
\begin{itemize}
	\item Utiliser les histogrammes de couleurs des pixels de l'objets et des pixels du fond afin d'en déduire des distributions empiriques $\Pp_{emp}$.
	\item Utiliser des mixtures de gaussiennes.
	\item Faire des comparaisons de patchs de texture.
\end{itemize}

\paragraph{Terme de régularisation / A priori}
On suppose que $f$ est un champ Markovien par rapport au voisinage $\mathcal{N}$. C'est à dire :
$$ \Pp(f_p = x \mid f_{\mathcal{P} \setminus \{ p \}}) = \Pp(f_p = x \mid f_{\mathcal{N}_p}) $$
On dit que $X = \left( X_p \right)_{p \in \mathcal{P}}$ est un champs de Gibbs par rapport au graphe $\mathcal{G}$ si :
$$ \Pp(X = x) \propto \exp \left( - \sum_{C \text{ clique de } \mathcal{G}} V_C(x) \right) $$

\PROP[ (Hammersley-Clifford 1971)]{
	Si la distribution de probabilité de $X$ est strictement positive. C'est à dire que pour tout $x$, $\Pp(X > 0) > 0$. Alors $X$ est un champ Markovien par rapport à $\mathcal{N}$ si et seulement si $X$ est un champ de Gibbs par rapport à $\mathcal{N}$
}

Dans notre cas $f$ est une distribution strictement positive. Ainsi $f$ est un champ Markovien et les seuls cliques de $\mathcal{N}$ sont ses arrêtes. D'où :
$$ R(f) = - \log \Pp(f) = - \log \exp \left( - \sum_{p, q \in \mathcal{N}} V_{p, q}(f) \right) = \sum_{p, q \in \mathcal{N}} V_{p, q}(f_p, f_q) $$
De plus on suppose la symétrie des potentiels. Ainsi il existes des réels $B_{p, q}$ tel que :
$$ V_{p, q}(f_p, f_q) = B_{p, q} \, \left( 1 - \delta_{f_p, f_q} \right) $$
Où $\delta$ est ici le symbole de Kronecker. On prendra généralement l'expression suivante de la constante pour pénaliser la continuité de l'intensité :
$$ B_{p, q} = \exp \left( - \dfrac{(I_p - I_q)^2}{2 \sigma^2} \right) \; / \; \dist(p, q) $$
Cela revient à peu près à pénaliser les faibles gradients :
$$ B_{p, q} = g \left( \left\| \nabla I_p \right\| \right) $$
Où $g$ est une fonction strictement positive et décroissante. Par exemple $g(x) = 1 / (1 + cx^2)$.

\paragraph{Conclusion}
On cherche donc à minimiser l'énergie suivante (ou toute autre fonctions similaire) :
$$ E(f) = \sum_{p \in \mathcal{P}} \Pp_{emp}(I_p \mid f_p) \; + \lambda \; \sum_{p, q \in \mathcal{N}} \left( 1 - \delta_{f_p, f_q} \right) \exp \left( - \dfrac{(I_p - I_q)^2}{2 \sigma^2} \right) \; / \; \dist(p, q) $$
Où :
$$ \Pp_{emp}(c \mid l) = \syst{ll}{
	\dfrac{\# \left\{ p \in \mathcal{O} \mid I_p = c \right\}}{\# \mathcal{O}} & \text{Si } l = 1 \\[2mm]
	\dfrac{\# \left\{ p \in \mathcal{B} \mid I_p = c \right\}}{\# \mathcal{B}} & \text{Si } l = 0
} $$
On remarque alors qu'un pixel de $\mathcal{O}$ n'est pas nécessairement assigné au label 1 alors qu'il devrait l'être. On modifie alors légèrement $D_p$ de la manière suivante :
$$ D_p(l) = \syst{ll}{
(+ \infty) \, . \, (1 - l) & \text{Si } p \in \mathcal{O} \\
(+ \infty) \, . \, l & \text{Si } p \in \mathcal{B} \\
\Pp_{emp}(I_p \mid l) & \text{Sinon}
} $$

\sect{Coupes de graphes multi-labels}

Cette fois-ci notre ensemble de labels n'est plus $\{ 0, 1\}$ mais un ensemble $\mathcal{L}$ fini. L'énergie a toujours la même expression, à savoir :
$$ E(f) = \sum_{p \in \mathcal{P}} D_p(f_p) \; + \; \sum_{p, q \in \mathcal{N}} V_{p, q}(f_p, f_q) $$

\PROP[ (Ishikawa 2003)]{
	Si les $V_{p, q}$ sont convexes et si $\mathcal{L}$ est linéairement ordonné (c'est à dire que $\mathcal{L}$ se trouve dans un espace à une seule dimension) alors une labellisation optimale $f^*$ peut être obtenue en utilisant la coupe minimale d'un graphe.
}

La convexité impose le fait qu'il est plus rentable de faire des petits sauts de labels de proche en proche plutôt que des gros saut de labels :
\begin{center}
	\begin{tikzpicture}
		\draw (0, 0) rectangle (4, 0.5);
		\foreach \x in {0.5, 1, ..., 4} {
			\draw (\x, 0) -- (\x, 0.5);
			\pgfmathtruncatemacro{\l}{2 * \x};
			\node at ({\x-0.25}, 0.25) {\l};
		}
		\draw (5, 0) rectangle (9, 0.5);
		\foreach \x in {5.5, 6, ..., 8.5}
			\draw (\x, 0) -- (\x, 0.5);
		\foreach \x in {5.25, 5.75, ..., 6.75}
			\node at (\x, 0.25) {1};
		\foreach \x in {7.25, 7.75, ..., 8.75}
			\node at (\x, 0.25) {8};
		\node at (2, -0.4) {faible coût};
		\node at (7, -0.4) {fort coût};
	\end{tikzpicture}
\end{center}
Les changement de labels sont alors continues et lisses. Il peut arriver que l'on ai besoin de fortes discontinuités dans les labels. Dans ce cas on est obligé de prendre $V_{p, q}$ non convexe. Le théorème de Ishikawa ne tient alors plus mais en général on connaît de bon algorithmes d'approximations de la solution optimale. Il faut savoir que dans la plupart des cas non convexes, le problème est NP-difficile. On peur par exemple prendre la fonction suivante :
\begin{center}
	\begin{tikzpicture}
		\draw[->] (0, -0.1) node[below] {$l_p$} -- (0, 2);
		\draw[->] (-2.2, 0) -- (2.2, 0) node[right] {$l_q$};
		\draw[red, very thick, smooth, variable=\x, domain=-1.1:1.1]
			plot (\x, {\x * \x * 1.5 / 1.1 / 1.1});
		\draw[red, very thick] (-2, 1.5) -- (-1.1, 1.5);
		\draw[red, very thick] (2, 1.5) -- (1.1, 1.5);
		\node[right] at (2.2, 1) {$V_{p, q}(l_p, l_q) = \min \left( K, \| l_p - l_q \|^2 \right)$};
	\end{tikzpicture}
\end{center}

\paragraph{Résolution}
On suppose $\mathcal{L} = \{ 1, ..., k \}$ et on suppose que le terme de régularisation est de la forme :
$$ V_{p,q}(f_p, f_q) = \lambda_{p, q} | f_p - f_q | $$
L'idée est de se ramener à une coupe binaire en créant une couche de nœuds par label :
\begin{center}
	\begin{tikzpicture}[gr/.style={fill opacity=0.33, circle, draw=black, text opacity=1, minimum size=8mm}, scale=1.22]
		\node[gr, fill=red] (s) at (0, 0) {$s$};
		\node[gr, fill=blue] (t) at ({1.7*4}, 0) {$t$};
		\foreach \x in {1, 2, 3} {
			\node[gr, fill=gray] (p\x) at ({1.7*\x}, 0.8) {$p_\x$};
			\node[gr, fill=gray] (q\x) at ({1.7*\x}, -0.8) {$q_\x$};
		}
		\foreach \x in {2, 3} {
			\pgfmathtruncatemacro{\y}{\x-1};
			\draw[->] (p\y) -- node[above] {$w_\x^p$} (p\x);
			\draw[->] (q\y) -- node[below] {$w_\x^q$} (q\x);
			\draw[<->] (p\x) -- node[right, pos=0.7] {$\lambda_{p, q}$} (q\x);
		}
		\draw[->] (s) edge node[above] {$w_1^p$} (p1) edge node[below] {$w_1^q$} (q1);
		\draw[<-] (t) edge node[above] {$w_4^p$} (p3) edge node[below] {$w_4^q$} (q3);
		\draw[<->] (p1) -- node[right, pos=0.7] {$\lambda_{p, q}$} (q1);
		\node at (0.8, 1.7) {Cas $k = 4$};
		\node[greenTikz] at (5.5, 1.7) {Coupe $f_p = 3, \, f_q = 1$};
		
		\draw[greenTikz, dashed, very thick] (0, -0.66) to[out=0, in=-150, distance=8mm] (1.3, 0) to[out=30, in=-150, distance=5mm] (4, 0.3) to[out=30, in=-170, distance=8mm] (5.02, 1.3) to[out=10, in=120, distance=7mm] (7, 0.8);
	\end{tikzpicture}
\end{center}
Le label associé à la coupe est alors défini par la position de la coupe entre les nœuds $s, p_1, p_2, ..., p_{k-1}, t$. Reste à choisir les poids $w_l^p$. On peut dans un premier temps penser à :
$$ w_l^p = D_p(l) $$
Hélas avec ce poids on peut obtenir une coupe minimale qui coupe plusieurs fois une même ligne :
\begin{center}
	\begin{tikzpicture}[gr/.style={fill opacity=0.33, circle, draw=black, text opacity=1, minimum size=8mm}, scale=1.1]
	\node[gr, fill=red] (s) at (0, 0) {$s$};
	\node[gr, fill=blue] (t) at ({1.7*4}, 0) {$t$};
	\foreach \x in {1, 2, 3} {
		\node[gr, fill=gray] (p\x) at ({1.7*\x}, 0.8) {$p_\x$};
		\node[gr, fill=gray] (q\x) at ({1.7*\x}, -0.8) {$q_\x$};
	}
	\foreach \x in {2, 3} {
		\pgfmathtruncatemacro{\y}{\x-1};
		\draw[->] (p\y) -- (p\x);
		\draw[->] (q\y) -- (q\x);
		\draw[<->] (p\x) -- (q\x);
	}
	\draw[->] (s) edge (p1) edge (q1);
	\draw[<-] (t) edge (p3) edge (q3);
	\draw[<->] (p1) -- (q1);
	
	\draw[red, dashed, very thick] (0, -0.66) to[out=0, in=-180, distance=8mm] (1.7, 0) to[out=0, in=180, distance=9mm] (3.4, 1.35) to[out=0, in=140, distance=10mm] (4.6, -1.15) to[out=-40, in=20, distance=7mm] (6.7, -1);
	\end{tikzpicture}
\end{center}
Une solution consiste à ajouter une grande constante à tous les poids $w_l^p$ afin que la coupe ne passe que par une seule de ces arrêtes pour chaque ligne. On pose alors :
$$ w_l^p = D_p(l) + K_p \qquad \text{Avec} \; K_p = 1 + (k - 1) \sum_{q \in \mathcal{N}_p} \lambda_{p, q} $$

\sect{Carte de disparités}

On se donne deux images rectifiées $I$ et $I'$. On cherche à trouver la disparité $d_p$ de chaque pixel de $I$. On a donc $d_p \in \mathcal{L} = \{ d_{min}, ..., d_{max} \}$. Ensuite on veut $D_p(d_p)$ petit lorsque $I_p$ est proche de $I'_{p + d_p e_1}$ et aussi $V_{p, q}(d_p, d_q)$ petit lorsque $d_p$ et $d_q$ sont proches. Ainsi pour $D_p(d_p)$ on prend n'importe quel distance entre les patchs de l'images $I$ à la position $p$ et de l'image $I'$ à la position $p + d_pe_1$ vu au chapitre précédent C.\ref{s:disp}. Pour la régularisation on prend de même l'expression utilisé dans la résolution de la section précédente. Cela donne par exemple :
$$ E(f) = \sum_{p \in \mathcal{P}} D_{ZCSSD} \left( I_{P + p}, \, I'_{P + p + d_p e_1} \right) \; + \; \sum_{p, q \in \mathcal{N}} \lambda_{p, q} | d_p - d_q | $$
Où NCSSD signifie \textit{Normalized Centered Sum of Square Differences} :
$$ D_{ZCSSD} \left( I, I' \right) = \sum_p \left( \left( I_p - \bar{I} \right) / \sigma_I - \left( I'_p - \bar{I'} \right) / \sigma_{I'} \right)^2 $$

\paragraph{Approximation}
En pratique optimiser sur toute l'image nécessite beaucoup de nœuds. C'est pourquoi une approximation consiste à séparer l'image en sous-problème et optimiser de manière exacte localement tous ces nouveaux sous-problèmes.

\paragraph{Déplacement de labellisations}
Soit une labellisation $f : \mathcal{P} \rightarrow \mathcal{L}$ et des labels $\alpha$ et $\beta$.
\begin{itemize}
	\item On dit que $f'$ est un \textbf{déplacement standard} depuis $f$ si $f$ et $f'$ diffère en au plus un pixel.
	\item On dit que $f'$ est une $\boldmath{\alpha}$\textbf{-expansion} depuis $f$ si pour tout pixel $p$ la valeur de $f'_p$ est soit $f_p$ soit $\alpha$.
	\item On dit que $f'$ est une $\boldmath{\alpha}$\textbf{-}$\boldmath{\beta}$\textbf{-permutation} depuis $f$ si pour chaque pixel $p$ alors soit $f_p = f'_p$ soit $(f_p, f'_p) \in \{ \alpha, \beta\}^2$.
\end{itemize}
Une technique d'optimisation consiste à itérer ces déplacement pour converger vers un minimum local. Les deux derniers déplacement présenté ci-dessous peuvent être optimisés avec une coupe de graphe binaire ce que l'on sait très bien faire.

\paragraph{Occlusion}
On appelle pixel d'occlusion les pixels qui n'apparaissent que dans une seule des images. Pour trouver les pixels d'occlusion on peut calculer les cartes de disparité sur les deux images $I$ puis $I'$ et ensuite vérifier que si un pixel $p$ de $I$ est envoyé vers un pixel $q$ de $I'$ alors $q$ est envoyé vers $p$. Si ce n'est pas le cas alors $p$ est un pixel d'occlusion.