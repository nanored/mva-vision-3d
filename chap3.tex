\chapter{Carte de disparités}

\myminitoc

\sect{Triangulation}

\paragraph{}
Commençons par reprendre les formules binoculaire pour observer un point de l'espace $X$ avec des caméras de matrices de projections $P$ et $P'$. Les coordonnées homogènes de l'images de $X$ par les deux caméras sont notées $x$ et $x'$ :
$$ x = PX \Leftrightarrow [x]_\times PX = 0 \qquad x' = P'X \Leftrightarrow [x']_\times P'X = 0 $$
L'objectif de la triangulation est à partir des deux images $x$ et $x'$, de retrouver le point dans l'espace $X$. Cela est faisable en calculant une SVD pour trouver le noyau d'une matrice :
$$ X \in \ker \pmat{[x]_\times P \\ [x']_\times P'} $$

\paragraph{}
En se plaçant dans le repère de la seconde caméra et en notant $R$ la matrice de rotation du repère de la seconde caméra au repère de la première caméra et $T$ la translation entre les deux caméras. On peut réécrire les équations précédentes avec :
$$ \lambda x = K(RX + T) \qquad \lambda' x' = K' X $$
On pose alors le vecteur $Y = \pmat{X^\trans & 1 & \lambda & \lambda'}^\trans$ qui vérifie le système suivant :
$$ \pmat{KR & KT & -x & 0 \\ K' & 0 & 0 & -x'} Y = 0 $$
On retrouve alors $X$ à l'aide de la SVD de la matrice de droite qui nous donne le noyau.

\paragraph{Récupération de $R$ et $T$}
Supposons que nous connaissons les matrices de calibration $K$ et $K'$ en plus de la matrice essentielle $E$ (obtenable à partir de la matrice fondamentale $F$ grâce à $K$ et $K'$). On essaye alors de retrouver $R$ et $T$. On sait que $E = \left[ T \right]_\times R$.
$$ E^\trans E y = R^\trans [T]_\times^\trans [T]_\times R y = - R^\trans [T]_\times \left([T]_\times (R y) \right) = - R^\trans \left( (T^\trans (R y)) T - (T^\trans T) (Ry) \right) $$
Ce qui donne :
$$ E^\trans E = R^\trans \left( T T^\trans - \| T \|_2^2 I_3 \right) R = - x x^\trans + \| T \|_2^2 I_3 \qquad \text{Avec } x = R^\trans T $$
On observe alors que $E^\trans E x = 0$ et que pour $y$ orthogonal à $x$, $E^\trans E y = \| T \|_2^2 y$. Ainsi $E^\trans E$ est une matrice ortho-diagonalisable de valeur propres $\| T \|_2^2$ et $0$ et de rang 2. Ainsi les valeurs propres de $E$ sont $\sigma_1 = \sigma_2 = \| T \|_2 = \sigma$ et $\sigma_3 = 0$. La SVD de $E$ donne :
$$ E = U \pmat{\sigma \\ & \sigma \\ & & 0 } V^\trans = \sigma U \pmat{0 & -1 & 0 \\ 1 & 0 & 0 \\ 0 & 0 & 0} U^\trans U \pmat{0 & 1 & 0 \\ -1 & 0 & 0 \\ 0 & 0 & 1} V^\trans = \left[ T \right]_\times R $$
On identifie alors :
$$ \left[ T \right]_\times = \pm \sigma U \pmat{0 & -1 & 0 \\ 1 & 0 & 0 \\ 0 & 0 & 0} U^\trans \qquad R = \pm U \pmat{0 & 1 & 0 \\ -1 & 0 & 0 \\ 0 & 0 & 1} V^\trans $$
Ce qui donne 4 solutions possibles :
$$ \left\{ \begin{array}{lll}
T & = & \pm \sigma U [e_3]_\times U^\trans \\[1mm]
R & = & \pm U R_z \left( \pm \frac{\pi}{2} \right) V^\trans
\end{array} \right. $$

\sect{Carte de disparités}
\label{s:disp}

On cherche à déterminer la profondeur du point observé en chaque pixel d'une image. Pour cela on se sert d'une autre image de la même scène prise à une position légèrement différente.
\begin{center}
	\begin{tikzpicture}[scale=0.8]
		\draw (-1, 0) -- (0, 0) -- node[above] {$B - d$} (2.25, 0) -- (4, 0) node[right] {plan image};
		\draw[thick] (0, -1)node {$\bullet$} -- node[left] {$f$} (0, 0) node {$\bullet$} -- node[left] {$z-f$} (0, 3) node {$\bullet$};
		\draw[dashed] (-1.5, -1) -- node[left] {$z$} (-1.5, 3);
		\draw[dashed] (2.25, 0) -- (2.25, 1) -- node[above] {$d$} (3, 1) -- (3, -1);
		\draw (0, -1) node[below] {$C$} -- node[below] {$B$} (3, -1) node {$\bullet$};
		\draw[thick] (3, -1) node[below] {$C'$} -- (2.25, 0) node {$\bullet$} -- (0, 3) node[above] {$M$};
	\end{tikzpicture}
\end{center}
Avec un simple théorème de Thalès on a que la profondeur $z$ d'un point $M$ est inversement proportionnel à la disparité $d$ de ce point, lorsque l'on bouge la caméra d'une distance $B$ :
$$ \dfrac{B-d}{B} = \dfrac{z-f}{z} \Leftrightarrow z = \dfrac{f B}{d} $$
A partir de quelques points caractéristiques de l'image on est capable de déterminer des bornes sur les valeurs que peut prendre $d$. Cela facilitera énormément le calcul de $d$ pour tous les pixels de l'image car on pourra borné la zone de recherche pour trouver la nouvelle position de chaque pixel. Le principal défi se trouve au niveau du choix de l'invariant que l'on considère pour identifier la nouvelle position d'un pixel (exemple : la couleur, le gradient selon l'axe $x$, ...). Enfin on distingue deux méthodes de résolution du problème. Les méthodes globales et celles locales.

\paragraph{Méthode globale}
On cherche une image de disparité $d^*$ solution du problème :
$$ d^* = \argmin_d \sum_p E_{data} \left( p, p + d(p); I_L, I_R \right) + \sum_{p \sim p} E_{reg} \left( d(p), d(p'); p, p', I_L, I_R \right) $$
Où $p \sim p'$ désigne l'ensembles des paires de pixels voisins. $E_reg$ permet d'imposer une certaine régularité dans la carte de disparité. On peut par exemple prendre :
$$ E_{reg} = \left| d(p) - d(p') \right|^2 $$
En revanche à la frontière entre deux objets de profondeur différente, on ne souhaite pas une telle régularité. Les frontière sont généralement caractérisé par un changement de couleur entre deux pixels voisins. Voici donc une autre possibilité le coût de régularisation :
$$ E_{reg} = \exp \left( - \frac{1}{\sigma^2} \| I_L(p) - I_L(p') \|^2 \right) \left| d(p) - d(p') \right| $$
Malheureusement ce problème est NP-difficile pour presque tous les termes de régularité (sauf $E_{reg} = \lambda_{p, p'} \left| d(p) - d(p') \right|$). Il faut donc utiliser des approximations.

\paragraph{Méthode locale}
Ces méthodes consiste à comparer des patchs autour des pixels. Voici par exemple des distances sur des patchs $P$ :
$$ \text{SAD (Sum of Absolute Differences) :} \quad D(p, q) = \sum_{r \in P} \left| I_L(p+r) - I_R(q+r) \right| $$
$$ \text{SAD (Sum of Squared Differences) :} \quad D(p, q) = \sum_{r \in P} \left| I_L(p+r) - I_R(q+r) \right|^2 $$
Augmenter la taille de $P$ permet une plus grande régularité car la proportion de pixel en commun entre $p + P$ et $p' + P$ pour $p$ et $p'$ voisins, est plus grande. En revanche augmenter la taille du pixel risque d'effacer de petits objets. \\
Un problème qui peut intervenir est un changement d'intensité lumineuse entre l'image de gauche et celle de droite. Pour remédier à ce problème il est possible de soustraire la moyenne d'intensité sur le patch à l'intensité de chaque pixel. On peut même normalisé par la moyenne et la variance pour être encore plus robuste. Cela conduit aux de nouvelle carte de disparité suivante:
$$ \text{CSSD (Centered Sum of Squared Distances) :} \vspace{-3mm} $$
$$ d(p) = \argmin_d \sum_{r \in P} \left( I_L(p+r) - \overline{I_{L | p+P}} - I_R(p + de_1 + r) + \overline{I_{R | p+de_1+P}} \right)^2 $$
$$ \text{NCC (Normalized Cross-Correlation) :} \vspace{-1mm} $$
$$ d(p) = \argmax_d \dfrac{\sum_{r \in P} \left( I_L(p+r) - \overline{I_{L | p+P}} \right) \left( I_R(p + de_1 + r) - \overline{I_{R | p+de_1+P}} \right)}{\sqrt{\sum \left( I_L(p+r) - \overline{I_{L | p+P}} \right)^2} \sqrt{\sum \left( I_R(p + d e_1 + r) - \overline{I_{R | p+de_1+P}} \right)^2}} $$

\paragraph{Expansions de graines}
On peut obtenir des erreurs et ainsi du bruit dans notre carte de disparité. Pour remédier à cela une technique consiste à ne garder la disparité calculé que pour pour les pixels qui obtiennent une bonne corrélation ou une faible distance avec le patchs auquel ils sont associés. Tous ces pixels sont mis dans une file de priorité. Puis on déplie la file et pour chaque pixel $p$ de la file on recalcule la disparité de ses voisin $p' \sim p$ qui n'ont encore jamais été dans la file. Cette disparité est calculé en retenant la meilleur parmi $d(p)-1$, $d(p)$ et $d(p)+1$. Cela permet d'obtenir une certaine régularité et évite le bruit. De plus comme généralement les pixels dont on n'est pas trop sûr de la disparité se trouve dans des zones de faible gradient on impose généralement pas cette régularité aux frontières entre les objets.

\paragraph{Supposition}
On a supposé dans les calculs précédent qu'il n'y avait pas de rotation de la caméra et que la caméra se déplaçait uniquement suivant l'axe des $x$. Ces suppositions ne sont pas problématique car, comme vu dans le chapitre précédent, il est possible de réaliser une rectification épipôlaire pour se placer dans les conditions voulues.